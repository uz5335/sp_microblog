const fs = require('fs'),
    jwt = require('jsonwebtoken');
    stream = fs.createWriteStream('./api.log');
module.exports = function (req, res, next) {
    let userId = null;
    try {
        userId = jwt.decode(req.headers.authorization.split(' ')[1])._id;
    }
    catch (e){}
    stream.write(JSON.stringify({
        userId,
        time: Date.now(),
        body: req.body,
        url: req.url
    }) + '\n');
    next()
};