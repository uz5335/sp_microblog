# MicroBlog

Kot blog, samo manjše.


## Povezava na delujočo aplikacijo

Aplikacija je javno na voljo na naslovu: https://sp-microblog2017.herokuapp.com/

Na začetni
strani se uporabi za prikaz login.jade maska in vse njene pripadajoče JADE
maske, ki so znotraj navedene. Pot: app_server/views/login_register

## Namestitev/poganjanje
Za delovanje morate imeti nameščen MongoDB

* klonirajte projekt: git clone https://uz5335@bitbucket.org/uz5335/sp_microblog.git  
* prestavite se v mapo projekta: cd sp_microblog  
* namestite vse odvisnosti: npm install
* v c9 okolju posodobite nodeJS verzijo s sledečimi ukazi: nvm install 8.9.1,  nvm use 8.9.1,  nvm alias default v8.9.1
* nalozite nodemon: npm install nodemon -g  
* zazenite aplikacijo (lokalna podatkovna baza): nodemon
* zaženite aplikacijo (oddaljena podatkovna baza): NODE_ENV=production MLAB_URI=mongodb://user:pass@ds129386.mlab.com:29386/mikroblog nodemon

## Opis

To je spletna aplikacija, v kateri bodo ljudje lahko javno objavljali kratka
sporočila. Rešuje problem, da uporabniki ne morejo deliti svojih idej s svetom
na preprost način. Aplikacija omogoča iskanje sporočil po osebi, ki ga je
poslala ali temi sporočila (oznacena s #). Za uporabo je zahtevana prijava.
Uporabniki lahko na svojem računu nastavijo katere osebe jih zanimajo s
"followanjem" ali "unfollowanjem". S tem dobijo personaliziran "feed". Na njem
dobivajo spročila od uporabnikov, ki jih zanimajo in njihove retweete.

## Delovanje

Posebno bomo obravnavali znake kot so # in @. Beseda za tem bo pomenila oznako,
po kateri lako ostali uporabniki isčejo temo (#) ali drugega uporabnika (@). Če
uporabnik v iskalno vrstico vpiše #{nekaBeseda} se bodo prikazali vsi tweeti, ko
vsebujejo to oznako v kronološkem zaporedju (najnovejši na vrhu). V primeru da
napiše @{uporabniskoIme} se bo prikazal iskan uporabnik in njegovi tweeti. Ko
uporabnik nastavi komu sledi, bo na svojem domačem zaslonu videl njihove tweete.
Te tweete lahko tudi retweeta, kar pomeni da jih bodo videli tudi njegovi
sledilci.

## Pot skozi aplikacijo

### Splošno

Vsaka stran ima na vrhu "topbar", ki je vedno enak ko je uporabnik vpisan zato
ga v naslednjih poglavjih ne bom dodatno opisoval. Vsebuje ikono strani (ptica)
in ikono uporabnika (ter zraven njegovo uporabniško ime). Če uporabnik ni na
strani za vpis ali registracijo (če ni vpisan ga vse ostale strani preusmerijo
na stran za vpis), je vidno tudi polje za iskanje. Klik na ikono ptice ga vrne
na domačo stran, klik na uporabniško ime pa odpre meni z opcijami:

* My profile
* Settings
* Log out

Vedno, ko bo aplikacija razdeljena v več stolpcev se bodo te združili v manj
odvisno od velikosti naprave (npr. na telefonu bo zloženo v en stolpec).

### Login

Pred uporabo aplikacije se je potrebno prijaviti.

* Zaslonski maski prijave

![LoginDesktop](/docs/loginDesktop.png) ![LoginMobile](/docs/Mobile_login.png)

Proces je precej standarden. Uporabnik pride na Login stran z poljem za vnos
emaila/uporabniškega imena in gesla, ter možnostjo prijave z zunanjimi
storitvami (npr. Google). V primeru da uporabnik še ni registriran ima v topbaru
viden gumb za registracijo, ki ga popelje na podobno stran na kateri si
uporabnik lahko ustvari račun.

* Zaslonski maski registracije

![RegisterDesktop](/docs/registerDesktop.png)
![RegisterMobile](/docs/Mobile_register.png)

### Glavna stran

* Glavna stran, ki se uporabniku odpre ob prijavi

![MainDesktop](/docs/mainDesktop.png) ![MainMobile](/docs/Mobile_homescreen.png)

Takoj ko se uporabnik prijavi pride na svojo domačo stran. Tu vidi sporočila, ki
ga zanimajo. Tu naj bi preživel večino svojega časa.

Razdeljena je na tri stolpce:

* Sredinski - V sredinskem stoplcu je polje za objavo sporočil, pod njim pa
  njegov "feed" (tam so vidna vsa sporočila od uporabnikov, ki jim sledi in
  njihovi retweeti).

* Levi - v njem so podatki o uporabniku

* Desni - vsebuje priporočene uporabnike, ki bi jim lahko sledil.

S klikom na svojo sliko desno zgoraj se odpre meni preko katerega lahko odpre
svoje nastavitve ali se izpiše.

Nazaj na glavno stran lahko uporabnik pride kadarkoli s klikom na logo
aplikacije v "topbaru".

* zaslonska maska spustnega menija

![MainDesktopOpenWindow](/docs/mainDropDownDesktop.png)
![MainMobileOpenWindow](/docs/Mobile_homescreen_open_window.png)

### Iskanje

Ko uporabnik nastavi nek filter v polju za iskanje se in potrdi iskalni niz, se
vsebina na strani zamenja. Če je iskal uporabnika (@), se na levi strani opis
njegovega profila nadomesti s profilom iskanega uporabika, zgine polje za vnos
novega tweeta, feed se nadomesti s tweeti tisteg uporabnika, priporocila ostalih
uporabnikov pa ostanejo ista. Če je iskal neko temo (#), potem izgine tudi polje
z opisom uporabnika in priporočenimi uporabniki. Pokažejo se le tweeti z iskano
vsebino.

* Primer filtra za iskanje po temah

![#SearchMobile](/docs/hashatag_search_desktop.png)
![#SearchMobile](/docs/Mobile_hashtag_search.png)

Opis druega uporabnika ima za razliko od opisa trenutnega uporabnika tudi gumb
"Follow" ali "Unfollow" odvisno od tega če mu ze sledimo ali ne.

* Primer filtra za iskanje po osebah

![@SearchDesktop](/docs/@searchDesktop.png)
![@SearchMobile](/docs/Mobile_at_search.png)

### Tweet

Vsak tweet ima zgoraj levo sliko in ime tistega, ki jo je poslal, pod tem pa
vsebino sporocila.

Zgoraj desno so gumbi za opravljanje s tem sporočilom. Od leve proti desni:

* Retweet - Uporabnik s tem gumbom deli sporočilo tretjega uporabnika vsem
  svojim sledilcem. Vsebina in pošiljatelj ostaneta enaka.
* Favorite - Uporabnik lahko sporočilo "favorita" in s tem pokaže da mu je všeč.
* Link - Če bi uporabnik rad delil sporočilo lahko naredi s pomočjo tega gumba.
  Klik ustvari povezavo, ki jo potem lahko uporabnik deli.
* Zbriši - S tem lahko uporabnik izbriše svoje sporočilo ali retweet. Izbris
  izvornega sporočila izbriše tudi vse retweete.

### Spiski

Ob kliku na število sledilcev pri posameznem izpisku uporabnika se prikaže
seznam vseh sledilcev posameznega uporabnika. Zraven vsakega je gumb "Follow"
ali "Unfollow" odvisno od tega če mu že sledimo ali ne.

* Seznam vseh sledilcev od iskanega uporabnika

![followersDesktop](/docs/followersDesktop.png)
![followersMobile](/docs/Mobile_followers.png)

###Nastavitve

Tukaj ima uporabnik možnost spremeniti geslo in nastaviti ter spremeniti kratek
opis svojega profila. Le ta je prikazan pri opisu vsakega uporabnika. Z njim
imajo ostali hiter vpogled v to s čim se ta uporabnik ukvarja in/ali mu je
vredno slediti.

* Zaslonske maske nastavitev

![settingsDesktop](/docs/settingsDesktop.png)
![settingsDesktop](/docs/Mobile_settings.png)

## Primerjava izgleda aplikacije na različnih brskalnikih

Primerjali smo izgled naše aplikacije na različnih brskalnikih in sicer Google
Chrome, Mozilla Firefox in Microsoft Edge. Kolikor smo lahko opazili je edina
razlika med brskalniki to, da Microsoft Edge v polju za iskanje na začetku
(predenj vpišemo kakšen tekst) postavi cursor na levo in ga šele kasneje
prestavi na sredino. Poleg tega v to polje tudi doda X, s katerim lahko
pobrišemo to polje.

## JMeter testiranje

Kot je razvidno iz grafov v mapi /docs/JMeter, ki podajajo odzivne čase za različno količnino uporabnikov, so grafi do 500 uporabnikov pribljižno v enakem razmerju, nato pa pride do popačenj. Od 500 uporabnikov dalje, je bilo tudi že na oko v konzoli vidno, da je odziv veliko počasnejši.. Podrobne številke za vsak test pa so vidne v datotekah poleg grafov.

Najdaljši čas za odziv strežnika je pričakovano pri prijavi uporabnika v sistem, saj je v ozadju izvede preverjanje pristnosti in generiranje "JWT tokena". Od strani največ časa potrebuje začetna stran, saj se vse datoteke hkrati prenesejo na odjemalsko stran, kasneje pa se prenašajo samo še podakti (pričakovan rezultat, glede principa delovanja SPA).


## ZAP testiranje

V mapi /docs/ZAP so notri zaslonske maske ZAP testiranja. Tako "spider" in "active scann" ne najdeta nobene napake.. Sicer active scan je v enem trenutku našel SQL injection na loginu, vendar po ponovnem pregledu API-ja s klici kar se tiče uporabnika ga pa ni našel, pa tudi v naši aplikaciji niso možni (uporaba mongoDB, ki ne uporablja SQL sintakse) ter SQL injection napad se je bil zapisan kot del parametrov url naslova za vprašajem, le teh pa v loginu nikjer ne uporabimo, zato bi temu rekli "false flag".

## Čas nalaganja strani

Časi nalaganja so sledeči (uporabljam ista imena kot brskalnik):

firefox:

login - Dokončano: 563 ms	DOMContentLoaded: 283 ms	load: 283 ms

register - 2560ms - Dokončano: 1,71 s 	DOMContentLoaded: 335 ms	load: 335 ms

home - Dokončano: 896 ms     DOMContentLoaded: 268 ms       load: 268 ms

specific user - Dokončano: 952 ms 	DOMContentLoaded: 329 ms 	load: 330 ms

search - Dokončano: 1,24 s		DOMContentLoaded: 319 ms	load: 319 ms

profile - Dokončano: 752 ms 	DOMContentLoaded: 301 ms 	load: 301 ms


firefox - noscript

login - Dokončano: 185 ms 	DOMContentLoaded: 169 ms 	load: 232 ms

home - Dokončano: 1,31 s 	DOMContentLoaded: 1,33 s 	load: 1,35 s


chrome:

login - Dokončano: 181 ms	DOMContentLoaded: 146 ms	load: 146 ms

register - 2560ms - Dokončano: 438 ms 	DOMContentLoaded: 142 ms	load: 145 ms

home - Dokončano: 356 ms     DOMContentLoaded: 141 ms       load: 143 ms

specific user - Dokončano: 369 ms 	DOMContentLoaded: 148 ms 	load: 147 ms

search - Dokončano: 374 s		DOMContentLoaded: 146 ms	load: 148 ms

profile - Dokončano: 290 ms 	DOMContentLoaded: 152 ms 	load: 152 ms


chrome - noscript:

login - Dokončano: 45 ms 	DOMContentLoaded: 68 ms 	load: 78 ms

home - Dokončano: 1,42 s 	DOMContentLoaded: 1,44 s 	load: 1,46 s


Najdlje se nalaga podstran register zaradi zunanje storitve - Google reCaptha.
