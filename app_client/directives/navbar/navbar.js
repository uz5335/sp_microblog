(function() {
  /* global angular */
  
  var navbar = function() {
    return {
      restrict: 'EA',
      templateUrl: "/directives/navbar/navbar.html",
    };
  };
  
  angular
    .module('microblog')
    .directive('navbar', navbar);
})();