(function() {
  /* global angular */
  
  var userinfo = function() {
    return {
      restrict: 'EA',
      scope: {
        user: '=user'
      },
      templateUrl: "/directives/userInfo/userInfo.html",
      controller: 'userInfoCtrl',
      controllerAs: 'uvm'
    };
  };
  
  angular
    .module('microblog')
    .directive('userinfo', userinfo);
})();