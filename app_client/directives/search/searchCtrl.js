(function() {
  /* global angular */
  
  searchCtrl.$inject = ['$location', 'authentication', 'api' ,'$route'];
 function searchCtrl($location, authentication,api, $route) {
    var svm = this;
    svm.what_searched ="";
    svm.search = function(keyevent){
        if(keyevent.which == 13){
            svm.what_searched = svm.what_searched.trim();
            if(svm.what_searched){
                //console.log(svm.what_searched);
                let firstChar = svm.what_searched.charAt(0);
                if(firstChar === '@' || firstChar === "#"){
                    if(svm.what_searched.length > 1){
                        api.getUserByUsername(svm.what_searched.substr(1)).then(
                            function success(response){
                                if(response.data._id == authentication.getActiveUser()){
                                    $location.path("/home");
                                    $route.reload();
                                }else{
                                    $location.path("/search/"+svm.what_searched);
                                }
                            },
                            function error(err){
                                //console.log(err)
                                $location.path("/search/"+svm.what_searched);
                            }
                            );
                    }else{
                        getAlertModal("incomplete search request");
                        svm.what_searched = "";
                    }
                }else{
                    getAlertModal("you shoud search by @ or #");
                    svm.what_searched = "";
                }
            }else{
                getAlertModal("input field shoud not be empty");
            }
        }
    }

  };
  
  angular
    .module('microblog')
    .controller('searchCtrl', searchCtrl);
})();