(function() {
  /* global angular */
  
  var search = function() {

    return {
      restrict: 'EA',
      templateUrl: "/directives/search/search.html",
      controller: 'searchCtrl',
      controllerAs: 'svm'
    };
  };
  
  angular
    .module('microblog')
    .directive('search', search);
})();