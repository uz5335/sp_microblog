(function() {
  /* global angular */
  
  var header = function() {

    return {
      restrict: 'EA',
      scope: {
        content: '=content'
      },
      templateUrl: "/directives/loginAndRegisterHeader/header.html"
    };
  };
  
  angular
    .module('microblog')
    .directive('header', header);
})();