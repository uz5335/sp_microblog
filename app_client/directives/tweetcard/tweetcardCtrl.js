(function () {
    /* global angular */

    tweetcardCtrl.$inject = ['$location', 'api', 'authentication', '$route', '$scope'];
    function tweetcardCtrl($location, api, authentication, $route, $scope) {
        const tweet = $scope.tweet;
        this.tweet = tweet
        this.logOut = function () {
            authentication.logOut();
            $location.path("/");
            $route.reload();
        };

        //ugly, but it works
        function reloadAllTweets() {
            $scope.$parent.$parent.vm.getFeed();
        }

        this.myUserId = authentication.getActiveUser();

        this.whatLikes = -1;

        this.isMine = tweet.creator._id === authentication.tmpId();
        this.canLike = !this.isMine && !tweet.likes.includes(authentication.tmpId());
        this.canRetweet = !this.isMine && !tweet.retweets.some((entry) => {
            return entry.userId === authentication.tmpId()
        });
        this.canUnlike = tweet.likes.includes(authentication.tmpId());
        // console.log(this);
        if (this.canLike) {
            this.whatLikes = 1;
        } else if (this.canUnlike) {
            this.whatLikes = 2;
        } else {
            this.whatLikes = 0;
        }
        //console.log(this.canUnlike);

        this.removeTweet = () => {
            if (!this.isMine) return;

            var modal = getModal();
            modal.setContent('<h1>Really want to delete tweet?</h1>');

            modal.addFooterBtn('Cancel', 'tingle-btn tingle-btn--primary', function () {

                modal.close();
            });

            modal.addFooterBtn('Yes', 'tingle-btn tingle-btn--pull-right tingle-btn--danger', function () {
                api.deleteTweet(tweet._id, authentication.getActiveUser()).then(
                    function success() {
                        reloadAllTweets();
                    },
                    function error(err) {
                        console.log(err);
                    }
                );

                modal.close();
            });
            modal.open()

        };

        this.addLike = () => {
            if (!this.canLike) return;
            api.addLike(tweet._id, authentication.getActiveUser()).then(
                function success() {
                    reloadAllTweets();
                },
                function error(err) {
                    console.log(err);
                }
            );

        };

        this.addRetweet = () => {
            if (!this.canRetweet) return;
            api.addRetweet(tweet._id, authentication.getActiveUser()).then(
                function success() {
                    tweet.retweets.push(authentication.tmpId());
                    reloadAllTweets()
                },
                function error(err) {
                    console.log(err);
                }
            );

        }
        this.unlike = () => {
            if (!this.canUnlike) return;
            api.removeLike(tweet._id, authentication.getActiveUser()).then(
                function success() {
                    reloadAllTweets();
                },
                function error(err) {
                    console.log(err);
                }
            );

        }
        this.searchUser = function (username) {
            api.getUserByUsername(username).then(
                function success(response) {
                    if (response.data._id == authentication.tmpId()) {
                        $location.path("/home");
                        $route.reload();
                    } else {
                        $location.path("/search/@" + username);
                    }
                },
                function error(err) {
                    console.log(err);
                }
            );
        }
    }

    angular
        .module('microblog')
        .controller('tweetcardCtrl', tweetcardCtrl);
})();