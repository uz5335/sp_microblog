(function() {
    /* global angular */

    var tweetcard = function() {

        return {
            restrict: 'EA',
            scope: {
                tweet: '=tweet'
            },
            templateUrl: "/directives/tweetcard/tweetcard.html",
            controller: 'tweetcardCtrl',
            controllerAs: 'tvm'
        };
    };

    angular
        .module('microblog')
        .directive('tweetcard', tweetcard);
})();