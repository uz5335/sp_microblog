(function () {
    /* global angular */

    dropdownCtrl.$inject = ['$location', 'authentication', '$route'];
    function dropdownCtrl($location, authentication, $route) {
        var dropvm = this;
        dropvm.logOut = function () {
            var modal = getModal();
            modal.setContent('<h1>Are you sure you want to log out?</h1>');

            modal.addFooterBtn('Cancel', 'tingle-btn tingle-btn--primary', function () {

                modal.close();
            });

            modal.addFooterBtn('Yes', 'tingle-btn tingle-btn--pull-right tingle-btn--danger', function () {
                authentication.logOut();
                $location.path("/");
                $route.reload();
                modal.close();
            });
            modal.open()

        }
    };
    angular
        .module('microblog')
        .controller('dropdownCtrl', dropdownCtrl);
})();