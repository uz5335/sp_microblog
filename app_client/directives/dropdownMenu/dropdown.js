(function() {
  /* global angular */
  
  var dropdown = function() {
    return {
      restrict: 'EA',
      scope: {
        username: '=username'
      },
      templateUrl: "/directives/dropdownMenu/dropdown.html",
      controller: 'dropdownCtrl',
      controllerAs: 'dropvm'
    };
  };
  
  angular
    .module('microblog')
    .directive('dropdown', dropdown);
})();