(function () {
    /* global angular */
    angular.module('microblog', ['ngRoute', 'ngSanitize', 'vcRecaptcha']); //uporaba modula ngRoute (usmerjanje) in ngSaniteze (vključevanje html kode v angular app direktno); 
    function nastavitev($routeProvider, $locationProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'login/login.html',
                controller: 'loginCtrl',
                controllerAs: 'vm'
            })
            .when('/register', {
                templateUrl: 'register/register.html',
                controller: 'registerCtrl',
                controllerAs: 'vm'
            })
            .when('/home', {
                templateUrl: 'main/home/home.html',
                controller: 'homeCtrl',
                controllerAs: 'vm'
            })
            .when('/profile', {
                templateUrl: 'main/profile/profile.html',
                controller: 'profileCtrl',
                controllerAs: 'vm'
            })
            .when('/followers', {
                templateUrl: 'main/followers/followers.html',
                controller: 'followersCtrl',
                controllerAs: 'vm'
            })
            .when('/search/:searchString', {
                templateUrl: 'main/search/search.html',
                controller: 'searchCtrl1',
                controllerAs: 'vm'
            })

            .otherwise({redirectTo: '/'});

        $locationProvider.html5Mode(true);
    }


    angular
        .module('microblog')
        .config(['$routeProvider', '$locationProvider', nastavitev]);
})();