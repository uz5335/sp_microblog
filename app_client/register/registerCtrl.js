 (function(){
   
 /* global angular */
 angular
   .module('microblog')
   .controller('registerCtrl', registerCtrl);
  
 registerCtrl.$inject = ['$scope', 'authentication', '$location']; //imena tudi po tem ko izvedemo minifikacijo..
 function registerCtrl($scope, authentication, $location) {
    var vm = this;
     if(authentication.isLogIn()){
        $location.path('/home');
    }
    
    vm.info = {
        heading: "Join twitterium today",
        subheading: "Register"
    };
    
    vm.registerData = {
        username: "",
        password: "",
        passwordConfirm: "",
        email: "",
        recaptch: ""
    };
    
    vm.register = function(){
        vm.formError = "";
        //console.log(vm.registerData.recaptch);
        if(!vm.registerData.username || !vm.registerData.password || !vm.registerData.passwordConfirm || !vm.registerData.email){
            vm.formError = "all field are required";
        }else if(vm.registerData.password !== vm.registerData.passwordConfirm){
            vm.formError = "passwords do not match";
            vm.registerData.password = '';
            vm.registerData.passwordConfirm = '';
        }else if(!vm.registerData.recaptch){
            vm.formError = "check recaptcha...so we can be sure you are not robot";
        }
        else{
            console.log('uspesno');
            authentication.register({
                username: vm.registerData.username,
                password: vm.registerData.password,
                recaptch: vm.registerData.recaptch
            }).then(
               function success(){
                    if(authentication.isLogIn()){
                           $location.path('/home');
                    }
                 },
                 function error(err){
                     console.log(err);
                     vm.formError = err.data.error;
                 }
                );
            
        }
    }
    
  }
 })()
 