 (function(){
   
 /* global angular */
 angular
   .module('microblog')
   .controller('loginCtrl', loginCtrl);
  
 loginCtrl.$inject = ['$scope', '$location', 'authentication']; //imena tudi po tem ko izvedemo minifikacijo..
 function loginCtrl($scope, $location,authentication) {
    var vm = this;
     if(authentication.isLogIn()){
        $location.path('/home');
    }
    vm.info = {
        heading: "Welcome",
        subheading: "Log in"
    }
   vm.loginData = {
      username: "",
      password: ""
    };
    vm.error = false;
   
    vm.login = function(){
        vm.formError="";
        if(!vm.loginData.username || !vm.loginData.password){
            vm.formError = "All fields are required";
            console.log(vm.formError);
        
        }else{
            //potrebujem storitev avtetikacija, ki bo skrbela za login, register, logOut...
            //console.log(vm.loginData.username ," ", vm.loginData.password);
             vm.formError="";
             authentication.logIn(vm.loginData).then(
                 function success(){
                    if(authentication.isLogIn()){
                           $location.path('/home');
                    }
                 },
                 function error(err){
                     console.log(err);
                     vm.formError = err.data.error;
                 }
                 );
        }
    }
    
  }
 })()
 