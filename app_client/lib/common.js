"use strict";

function openModal(text, successFn, closeFn) {
    let obj = {
        modal: $("#modal"),
        modalBody: $("#modal-body").html(`<p>${text}</p>`),
        modalConfirmBtn: $("#modal-confirm"),
        modalCloseBtn: $("#modal-close"),

        confirmed: false,

        closeModal: function () {
            console.log("closing modal. was it confirmed?", this.confirmed);
            this.modal.off("hide.bs.modal");
            this.modalConfirmBtn.unbind();
            this.modalCloseBtn.unbind();
            if (!this.modal.hidden) this.modal.modal("hide");
            if (this.confirmed) return;
            closeFn();
        }
    };

    obj.modal.on("hide.bs.modal", obj.closeModal.bind(obj));
    obj.modalCloseBtn.click(obj.closeModal.bind(obj));

    obj.modalConfirmBtn.click(
        function () {
            console.log("modal confirm");
            this.confirmed = true;
            successFn();
            this.closeModal();
        }.bind(obj)
    );

    //this line actually opens the modal
    obj.modal.modal();

    return obj;
}

function getModal() {
    return new tingle.modal({
        footer: true,
        stickyFooter: false,
        closeMethods: ['overlay', 'button', 'escape'],
        closeLabel: "Close",
        cssClass: ['custom-class-1', 'custom-class-2'],
        onOpen: function () {
            console.log('modal open');
        },
        onClose: function () {
            console.log('modal closed');
        },
        beforeClose: function () {
            // here's goes some logic
            // e.g. save content before closing the modal
            return true; // close the modal
            return false; // nothing happens
        }
    });
}

function getAlertModal(message) {
    var modal = getModal();

    modal.setContent('<h1>' + message + '</h1>');

    modal.addFooterBtn('Okay', 'tingle-btn tingle-btn--pull-right', function () {
        modal.close();
    });

    modal.open();

}