(function () {
    /* global angular */

    function api($window, $http) {

        const getUser = (userId) => {
            return $http.get('/api/user', {
                    headers: {
                        Authorization: 'Bearer ' + userId
                    }
                }
            );
        };

        const getFeed = (userId) => {
            return $http.get('/api/feed'
                , {
                    headers: {
                        Authorization: 'Bearer ' + userId
                    }
                }
            );
        };

        const userDescriptionChange = function (newDescription, userId) {
            return $http.post('/api/user/changeDescription', {newDescription: newDescription}, {
                headers: {
                    Authorization: 'Bearer ' + userId
                }
            });
        };

        const changePassword = function (newPassword, oldPassword, userID) {
            return $http.post('/api/user/changePassword', {newPassword: newPassword, oldPassword: oldPassword}, {
                headers: {
                    Authorization: 'Bearer ' + userID
                }
            });
        };

        const deleteProfile = (userId) => {
            return $http.delete('/api/user', {
                headers: {
                    Authorization: 'Bearer ' + userId
                }
            });
        };

        const getAllRecommendations = (userId) => {
            return $http.get('/api/user/recommend'
                , {
                    headers: {
                        Authorization: 'Bearer ' + userId
                    }
                }
            );
        };

        const follow = (user_ID, follower_Id) => {
            let data = {
                userId: user_ID
            };
            return $http.post('/api/user/follow', data, {
                headers: {
                    Authorization: 'Bearer ' + follower_Id
                }
            });
        };

        const unfollow = (user_ID, follower_Id) => {
            let data = {
                userId: user_ID
            };
            return $http.post('/api/user/unfollow', data, {
                headers: {
                    Authorization: 'Bearer ' + follower_Id
                }
            });
        };

        const getAllFollowing = (user_ID) => {
            return $http.get('/api/user/following', {
                headers: {
                    Authorization: 'Bearer ' + user_ID
                }
            });
        };

        const deleteTweet = (tweetId, userID) => {
            return $http.delete('/api/tweet/' + tweetId, {
                headers: {
                    Authorization: 'Bearer ' + userID
                }
            });
        };

        const addLike = (tweetId, userId) => {
            return $http.post('/api/tweet/' + tweetId + '/like', {userId}, {
                headers: {
                    Authorization: 'Bearer ' + userId
                }
            });
        };


        const removeLike = (tweetId, userId) => {
            return $http.post("/api/tweet/" + tweetId + "/unlike", {userId}, {
                headers: {
                    Authorization: 'Bearer ' + userId
                }
            });
        };

        const addRetweet = (tweetId, userId) => {
            return $http.post('/api/tweet/' + tweetId + '/retweet', {userId}, {
                headers: {
                    Authorization: 'Bearer ' + userId
                }
            });
        };

        const createTweet = (userId, body) => {

            return $http.put('/api/tweet', {body}, {
                headers: {
                    Authorization: 'Bearer ' + userId
                }
            });
        };
        
        const getUserByUsername = (username) => {

            return $http.get("/api/user/username/" + username);
        };
        const search = (username, hashtag) => {
            return $http.get("/api/search?usernames=" + username + "&hashtags=" + hashtag);
        };

        return {
            getUser,
            getFeed,
            userDescriptionChange,
            changePassword,
            deleteProfile,
            getAllRecommendations,
            follow,
            deleteTweet,
            addLike,
            addRetweet,
            createTweet,
            getAllFollowing,
            unfollow,
            getUserByUsername,
            search,
            removeLike
        };
    }

    api.$inject = ['$window', '$http'];
    angular
        .module('microblog')
        .service('api', api);
})();