(function () {
    /* global angular */

    authentication.$inject = ['$window', '$http'];

    function authentication($window, $http) {

        var b64Utf8 = function (niz) {
            return decodeURIComponent(Array.prototype.map.call($window.atob(niz), function (c) {
                return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2)
            }).join(''));
        };


        let saveActiveUser = function (user_id) {
            $window.localStorage['client'] = user_id;
        }
        let getActiveUser = function () {
            return $window.localStorage['client'];
        }

        let logOut = function () {
            $window.localStorage.removeItem('client');
        }

        let logIn = function (userData) {

            return $http.post('/api/user/login', userData).then(
                function success(response) {
                    saveActiveUser(response.data);
                    console.log(isLogIn());
                });
        }

        let register = function (userData) {
            return $http.put('/api/user', userData).then(
                function success(response) {
                    saveActiveUser(response.data);
                    console.log(response.data);
                    //shraniZeton(odgovor.data.zeton);
                });
        }

        let isLogIn = function () {
            let token = getActiveUser();
            if (token) {
                //tukaj je narejen nastave za zeton..., kjer se bo vsebina zetona sparsala..
                //console.log(getActiveUser());
                var content = JSON.parse(b64Utf8(token.split('.')[1])); //odkodiranje koristne vsebine..
                return content.datumPoteka > Date.now() / 1000; //lahko recemo da je uporabnik prijavljen...
                return true;
            } else {
                return false;
            }
        }
        let tmpId = function () {
            return JSON.parse(b64Utf8(getActiveUser().split('.')[1]))._id;
        }
        return {
            logIn,
            getActiveUser: getActiveUser,
            logOut: logOut,
            isLogIn: isLogIn,
            register: register,
            tmpId: tmpId
        };
    }

    angular
        .module('microblog')
        .service('authentication', authentication);
})();