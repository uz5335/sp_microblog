(function () {

    /* global angular */
    angular
        .module('microblog')
        .controller('profileCtrl', profileCtrl);

    profileCtrl.$inject = ['$scope', '$location', '$route', 'authentication', 'api'];
    function profileCtrl($scope, $location, $route, authentication, api) {
        var vm = this;
        if (!authentication.isLogIn()) {
            $location.path('/');
            $route.reload();
        }
        vm.info = "";
        vm.userId = authentication.getActiveUser();
        let getUserProfiledata = function () {
            vm.info = "Preparing user data";
            api.getUser(vm.userId).then(
                function success(response) {
                    //console.log(response);
                    vm.user = response.data;
                    vm.info = "";
                },
                function error(err) {
                    console.log(err);
                }
            );
        }
        getUserProfiledata();

        vm.userDescription = "";
        vm.changeDescription = function () {
            vm.formErrorDescription = "";
            if (!vm.userDescription) {
                vm.formErrorDescription = "description shoud not be empty";
            } else {
                vm.info = "changing user description"
                //console.log("jej");

                api.userDescriptionChange(vm.userDescription, vm.userId).then(
                    function success(response) {
                        //console.log(response);
                        getUserProfiledata();
                        vm.userDescription = "";
                        vm.info = "";
                    },
                    function error(err) {
                        console.log(err);
                    }
                );
            }
        }

        vm.formPassword = {
            err: "",
            ok: ""
        }
        vm.password = {
            oldP: "",
            newP: "",
            confirmP: ""
        }
        vm.changePassword = function () {
            vm.formPassword.err = "";
            vm.formPassword.ok = "";
            if (!vm.password.oldP || !vm.password.newP || !vm.password.confirmP) {
                vm.formPassword.err = "all fields are required";
            } else if (vm.password.newP !== vm.password.confirmP) {
                vm.formPassword.err = "passwords do not match";
            } else {
                //poslji poizvedbo za sprembo gesla..
                let newPass = {
                    userId: vm.userId,
                    newPassword: vm.password.newP,
                    oldPassword: vm.password.oldP
                }
                vm.info = "changing password";
                api.changePassword(vm.password.newP, vm.password.oldP, vm.userId).then(
                    function success(response) {
                        vm.info = "";
                        vm.formPassword.ok = "Password change succes";
                    },
                    function error(err) {
                        vm.formPassword.err = "new and old pass do not match";
                        vm.info = "";
                    }
                );
            }
        }
        vm.deleteProfile = function () {

            var modal = getModal();
            modal.setContent('<h1>Really want to delete profile?</h1>');

            modal.addFooterBtn('Cancel', 'tingle-btn tingle-btn--primary', function () {

                modal.close();
            });

            modal.addFooterBtn('Yes', 'tingle-btn tingle-btn--pull-right tingle-btn--danger', function () {
                vm.info = "deleting profile";
                api.deleteProfile(vm.userId).then(
                    function success(response) {
                        vm.info = "";
                        authentication.logOut();
                        $location.path("/");
                        $route.reload();
                    },
                    function error(err) {
                        console.log(err);
                    }
                );
                modal.close();
            });
            modal.open()
        }


    }
})();
 