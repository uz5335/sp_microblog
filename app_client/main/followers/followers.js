 (function(){
   
 /* global angular */
 angular
   .module('microblog')
   .controller('followersCtrl', followersCtrl);
  
followersCtrl.$inject = ['$scope', '$location', '$route', 'authentication', 'api'];
function followersCtrl($scope, $location,$route, authentication, api) {
    var vm = this;
    if(!authentication.isLogIn()){
        $location.path('/');
        $route.reload();
    }
    vm.userId = authentication.getActiveUser();
    vm.info = "Preparing data";
    let getUserData = function(){
        
         api.getUser(vm.userId).then(
            function success(response){
                // console.log(response);
                vm.user = response.data;
                
            },
            function error(err){
                console.log(err);
            }
        );
    }
    let getUserFollowing = function(){
        
       api.getAllFollowing(vm.userId).then(
            function success(response){
                vm.allFollowing = response.data;
                //console.log(vm.allFollowing);
                vm.info = "";
            },
            function error(err){
                console.log(err);
            }
            );
    }
   
    getUserData();
    getUserFollowing();
 
    
    vm.unfollow = function(userId){
        //console.log(userId);
        vm.info = "Unfollow user";
        api.unfollow(userId, authentication.getActiveUser()).then(
            function success(response){
                vm.info = "preparing data";
                 getUserData();
                getUserFollowing();
                    
            },
            function error(err){
                console.log(err);
            }
            );
    }
    
   vm.searchUser = function(username){
        //console.log("clilkc", username);
        $location.path("/search/@"+username);
    }
    
  }
 })();
 