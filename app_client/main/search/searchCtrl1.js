 (function(){
   
 /* global angular */
 angular
   .module('microblog')
   .controller('searchCtrl1', searchCtrl1);
 searchCtrl1.$inject = ['$routeParams', '$scope', '$location', '$route', 'authentication', 'api'];
function searchCtrl1($routeParams,$scope, $location,$route, authentication, api) {
    var vm = this;
    if(!authentication.isLogIn()){
        $location.path('/');
        $route.reload();
    }
    vm.userId = authentication.getActiveUser();
    vm.info = "Preparing and searching in database";
    let getUser = function(){
        api.getUser(vm.userId).then(
            function success(response){
                // console.log(response);
                vm.user = response.data;
                //console.log(vm.user);
            },
            function error(err){
                console.log(err);
            }
        );        
    }

    getUser();
    let getRecommendations = function(searchUser){
        api.getAllRecommendations(vm.userId).then(
            function success(response){
                let recommendation1 = response.data;
                
                for (let i = 0; i < recommendation1.length; i++){
                    if(recommendation1[i].username === searchUser){
                        //console.log('yes');
                        vm.isFollowing = false;
                        return;
                    }
                }
                vm.isFollowing = true;
                // console.log(vm.recommendation);
            },
            function error(err){
                console.log(err);
            }
        );
    }
    
    
    vm.searchString = $routeParams.searchString;
    //console.log(vm.searchString);
    vm.User = "";
    if(vm.searchString.charAt(0) == "@"){
        vm.info = "searching for user";
        vm.User = vm.searchString.substr(1);
         getRecommendations(vm.searchString.substr(1));
        api.getUserByUsername(vm.searchString.substr(1)).then(
            function success(response){
              vm.User = response.data;
              //console.log(vm.User);
              vm.info = "";
            },
            function error(err){
                vm.User = "";
            }
            );
    }
    
    vm.follow = function(User_ID){
        vm.info = "following user";
        api.follow(User_ID, authentication.getActiveUser()).then(
            function success(response){
                getRecommendations(vm.searchString.substr(1));
                vm.info = "";
                api.getUserByUsername(vm.searchString.substr(1)).then(
                    function success(response){
                      vm.User = response.data;
                      //console.log(vm.User);
                      vm.info = "";
                    },
                    function error(err){
                        vm.User = "";
                    }
                    );
            },
            function error(err){
              console.log(err);
            }
        );
    }
    vm.unfollow = function(User_ID){
        //console.log(User_ID);
        vm.info = "unfollowing user";
         api.unfollow(User_ID, authentication.getActiveUser()).then(
            function success(response){
                getRecommendations(vm.searchString.substr(1));
                vm.info = "";
                api.getUserByUsername(vm.searchString.substr(1)).then(
                    function success(response){
                      vm.User = response.data;
                      //console.log(vm.User);
                      vm.info = "";
                    },
                    function error(err){
                        vm.User = "";
                    }
                    );
                
            },
            function error(err){
                console.log(err);
            }
        );
        
    }
    //PRIKAZ SE USTREZNIH TWEETOV..
    let getFeed = function(){
         vm.info = "getting tweets";
         let searchSting = "";
         vm.feedErr = "";
         vm.feed = "";
        if(vm.searchString.charAt(0) == "@"){
            
            searchSting = vm.searchString.substr(1);
            console.log(searchSting);
            api.search(searchSting, "").then(
                function success(response){
                    vm.feed = response.data;
                    //console.log(vm.feed);
                    if(vm.feed.length === 0){
                        vm.feedErr = "notrhing was found here";
                    }
                    vm.info = "";
                },
                function error(err){
                    console.log(err);
                }
                );
        }else if(vm.searchString.charAt(0) == "#"){
            searchSting = vm.searchString.substr(1);
            console.log(searchSting);
            api.search("", searchSting).then(
                function success(response){
                    vm.feed = response.data;
                    if(vm.feed.length === 0){
                        vm.feedErr = "notrhing was found here";
                    }
                    //console.log(vm.feed);
                    vm.info = "";
                },
                function error(err){
                    console.log(err);
                }
                );
        }
    
    }
    getFeed();
    this.getFeed = getFeed;
   
    
  }
 })();
 