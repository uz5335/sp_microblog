 (function(){
   
 /* global angular */
 angular
   .module('microblog')
   .controller('homeCtrl', homeCtrl);
  
homeCtrl.$inject = ['$scope', '$location', '$route', 'authentication', 'api'];
function homeCtrl($scope, $location,$route, authentication, api) {
    var vm = this;
    if(!authentication.isLogIn()){
        $location.path('/');
        $route.reload();
    }
    vm.userId = authentication.getActiveUser();
    //console.log(vm.userId);
    vm.info = "Preparing data";
    vm.tweetErr = ""
    this.createTweet = () => {
        const newTweetContent = $scope.vm.newTweetContent;
        vm.tweetErr = "";
        if (!newTweetContent ||
            newTweetContent.length === 0 ||
            newTweetContent.length > 250) {
                vm.tweetErr = "incorrect tweet format";
                return;
            }
        
        vm.info = "Creating new tweet";
        api.createTweet(this.userId, newTweetContent).then(
            function success(response){
                //console.log(response);
                vm.info = "";
                getFeed();
                vm.newTweetContent = "";
            },
            function error(err){
                console.log(err);
            }
            );
       // console.log(newTweetContent);
    };
    vm.isTweet = "";
    let getFeed = function(){
        vm.info = "updating user feed";
        vm.isTweet = "";
           api.getFeed(vm.userId).then(
            function success(response){
                vm.feed = response.data;
                if(vm.feed.length == 0){
                    vm.isTweet = "";
                }else{
                    vm.isTweet = "ok";
                }
                vm.info="";
                //console.log(vm.feed);
            },
            function error(err){
                console.log(err);
            }
        );
    }
    
    this.getFeed = getFeed;

    let getUser = function(){
        api.getUser(vm.userId).then(
            function success(response){
                // console.log(response);
                vm.user = response.data;
            },
            function error(err){
                console.log(err);
            }
        );    
    }
    
    let getRecommendations = function(){
        vm.isSommething = false;
        vm.info = "Preparing data";
        api.getAllRecommendations(vm.userId).then(
            function success(response){
                let recommendation1 = response.data;
                
                vm.recommendation = [];
                for (let i = 0; i < recommendation1.length; i++){
                    if(recommendation1[i]._id == authentication.tmpId()){
                        continue;
                    }
                    vm.recommendation.push(recommendation1[i]);
                    vm.isSommething = true;
                }
                vm.info = "";
                // console.log(vm.recommendation);
            },
            function error(err){
                console.log(err);
            }
        );
    }
    
    getFeed();
    getUser();
    getRecommendations();
    
    vm.follow = function(userID){
        vm.info = "following";
      api.follow(userID, authentication.getActiveUser()).then(
        function success(response){
            getFeed();
            getUser();
            getRecommendations();
        },
        function error(err){
          console.log(err);
        }
        );

    }
    vm.searchUser = function(username){
        //console.log("clilkc", username);
        $location.path("/search/@"+username);
    }
    
  }
 })();
 