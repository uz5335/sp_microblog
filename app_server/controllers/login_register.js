module.exports.login = function(req, res) {
    res.render("login_register/login", {
        //title: 'EduGeoCache - Poiščite zanimive lokacije blizu vas!',
        glavaStrani: {
            naslov: "Welcome",
            podnaslov: "Log in"
        },
        desniKot: "Register instead",
        link: "/register"
    });
};

module.exports.register = function(req, res, next) {
    res.render("login_register/register", {
        //title: 'Registracija' ,
        glavaStrani: {
            naslov: "Join twitterium today",
            podnaslov: "Register"
        },
        desniKot: "Login instead",
        link: "/login"
    });
};
