/* Vrni stran s podrobnostmi */
module.exports.home = function(req, res) {
    res.render("main/home", {});
};

module.exports.settings = function(req, res, next) {
    res.render("main/profile", {});
};

module.exports.followers = function(req, res, next) {
    res.render("main/followers", {});
};

module.exports.search = function(req, res, next) {
    res.render("main/search", {});
};
