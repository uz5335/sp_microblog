/* Vrni začetno stran s seznamom lokacij */
module.exports.login = function(req, res) {
    res.render("login_register/login", {
        //title: 'EduGeoCache - Poiščite zanimive lokacije blizu vas!',
        glavaStrani: {
            naslov: "Welcome",
            podnaslov: "Log in"
        },
        desniKot: "Register instead",
        link: "/register"
    });
};
