 var request = require('request');
 var apiParametri = {
   streznik: "http://localhost:" + process.env.PORT //lokalno poganjanje datoteke....
 };
 //nastavitve za produkcijsko okolje.
 if (process.env.NODE_ENV === 'production') {
   apiParametri.streznik = "https://sp-microblog2017.herokuapp.com";
 }

module.exports.home = function(req, res) {
        console.log(req.query.napaka);
        res.render("login_register/login", {
        //title: 'EduGeoCache - Poiščite zanimive lokacije blizu vas!',
        glavaStrani: {
            naslov: "Welcome",
            podnaslov: "Log in"
        },
        napaka: req.query.napaka,
    });
};

var getUserName = function(id, callback){
    let pot = '/api/user';
    let parametriZahteve = {
      url: apiParametri.streznik + pot,
      method: 'GET',
      json: {},
      headers: {
          Authorization: 'Bearer ' + id
        },
    };
    //console.log(parametriZahteve);
    request(
      parametriZahteve,
      function(err, response, data) {
          //console.log(response.statusCode, " ", data);
        if (response.statusCode == 200) {
            callback(null, data.username);
        } else {
          //potem si definitivno nekje neki zajebal in te kr vrzem nazaj nazaj na original...
            callback(err);      
        }
      }
    );
}

module.exports.mainPage = function(req, res) {
    let userId = req.params.userId;
    //console.log("drugi: ",userId);
    
    let pot = '/api/feed';
    let parametriZahteve = {
      url: apiParametri.streznik + pot,
      method: 'GET',
      json: {},
      headers: {
          Authorization: 'Bearer ' + userId
        },
    };
    
    //console.log(parametriZahteve);
    request(
      parametriZahteve,
      function(err, response, data) {
          //console.log(response.statusCode, " ", data);
        if (response.statusCode == 200) {
             getUserName(userId, function(error, username){
                 if(error){
                     res.redirect('/noscript?napaka=vrednost');
                 }else{
                     //console.log(username);
                    res.render("main/home", {
                        tweets: data,
                        username: username
                    });
                 }
             })
    
            
        } else {
          //potem si definitivno nekje neki zajebal in te kr vrzem nazaj nazaj na original...
          res.redirect('/noscript?napaka=vrednost');
        }
      }
    );
};

module.exports.logIn = function(req, res) {
    let username = req.body.username;
    let password = req.body.password;
    if(!password || !username){
        //takoj preusmeri ce kdo poizkusa kaj prevec..
        res.redirect('/noscript?napaka=vrednost');
        return;
    }
    let parametriZahteve, pot;
    //console.log(process.env.PORT);
     let loginData = {
      username: username,
      password: password
    };
    
    pot = '/api/user/login';
    parametriZahteve = {
      url: apiParametri.streznik + pot,
      method: 'POST',
      json: loginData
    };
    
    //console.log(parametriZahteve);
    request(
      parametriZahteve,
      function(err, response, data) {
          //console.log(response.statusCode, " ", data);
        if (response.statusCode == 200) {
            //console.log("prijavni: ", data);
            res.redirect('/noscript/mainPage/'+data);
        } else {
          res.redirect('/noscript?napaka=vrednost');
        }
      }
    );
    
};