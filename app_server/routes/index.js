var express = require("express");
var router = express.Router();

var login_register = require("../controllers/login_register.js");
var main = require("../controllers/main.js");



/*Login/Registracija */

router.get("/", function(req, res) {
    res.redirect("/home");
});

router.get("/register", login_register.register);
router.get("/login", login_register.login);

/* MainPage in vse kar je povezan s stem, torej uporabnik uporablja naso aplikacijo */
router.get("/home", main.home);
router.get("/profile", main.settings);
router.get("/followers", main.followers);
router.get("/search", main.search);

module.exports = router;
