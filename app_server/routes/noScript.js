var express = require("express");
var router = express.Router();

var noScript = require("../controllers/noScriptController.js");

router.get("/", noScript.home);
router.post("/login", noScript.logIn);
router.get("/mainPage/:userId", noScript.mainPage);
module.exports = router;