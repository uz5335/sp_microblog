"use strict";
const mongoose = require("mongoose");
const User = mongoose.model("User");
const Tweet = mongoose.model("Tweet");
const Description = mongoose.model("Description");
var bcrypt = require('bcrypt');
const saltRounds = 10;

function hash(password, cb) {
    bcrypt.genSalt(saltRounds, function (err, salt) {
        bcrypt.hash(password, salt, function (err, hash) {
            cb(hash)
        });
    });
}


function removeDescription(callback) {
    Description.remove({}, function (err, description) {
        if (err) {
            callback(err);
        } else {
            callback(null);
        }
    })
}

function removeTweets(callback) {
    Tweet.remove({}, function (err, tweet) {
        if (err) {
            callback(err);
        } else {
            callback(null);
        }
    })
}

module.exports.deleteAll = function (req, res) {
    User.remove({}, function (err, user) {
        if (err) {
            res.status(500).json(err);
            return;
        }
        removeDescription(function (error) {
            if (error) {
                res.status(500).json(error);
                return;
            }
            removeTweets(function (error1) {
                if (error1) {
                    res.status(500).json(error1);
                    return;
                }
                res.status(204).json({ok: "ok"});
            });
        });
    });
};


const initUsers = [
    {
        username: "user",
        password: "pass",
        description: "Kar je res je res. govorimo resnico, živeti življenje, kaj nam je mar...",
        tweets: [
            {
                body: "#123 prvi tweet"
            }
        ]
    },
    {
        username: "user1",
        password: "pass1",
        description: "jst sm pa res ful tečen in tega se zavedam, ampk to je moja prednost, ker se tega zavedam",
        tweets: [
            {
                body: "jst sm nogometas in to je cista resnica"
            },
            {
                body: "napisi ze enkrat ta READ.me"
            },
            {
                body: "uporaba async je je je je je #SP4life"
            }
        ]
    },
    {
        username: "user2",
        password: "pass2",
        description: "trgravl je najvišji vrh slovenije, ali je to resnica????",
        tweets: [
            {
                body: "oleole #SP4life"
            },
            {
                body: "nimas kej, #gremoNaprej"
            },
            {
                body: "vsak dan več dela"
            },
            {
                body: "amak re"
            },
            {
                body: "res res #123"
            }
        ]
    }
];


module.exports.init = function (req, res) {
    let count = 0;
    User.findOne({username: initUsers[0].username}).exec(function (err, user) {
        if (user) {
            res.status(500).json({error: "user already exists"});
            return;
        }
        else {

            for (let i = 0; i < initUsers.length; i++) {
                hash(initUsers[i].password, function (passHash) {

                    let user = new User({username: initUsers[i].username, password: passHash});
                    let description = new Description({body: initUsers[i].description, timeCreated: new Date()});
                    description.save(function (err, resD) {
                        //console.log("na novo shranjeujem!!!!", user, " \n\n", description, " ");
                        if (err) {
                            res.status(500).json({error: err});
                        }
                        else {
                            user.description = resD._id;
                            user.timeJoined = new Date();
                            //user.nastaviGeslo(initUsers[i].password);
                            //console.log("posodabljam uporabnika: ", user, "\n\n")
                            user.save(user, function (err, user) {
                                if (err) {
                                    //console.log("shranjevanje error")
                                    res.status(500).json({error: err});
                                }
                                initUserTweets(user._id, initUsers[i].tweets, function (err) {
                                    if (err) {
                                        res.status(500).json({error: err});
                                    }
                                });
                                count++;
                                if (count === initUsers.length) {
                                    res.status(200).json({ok: "ok"});
                                }
                            })
                        }
                    });
                })
            }
        }
    });
}

function initUserTweets(user_id, tweets, callback) {
    for (let i = 0; i < tweets.length; i++) {
        let tweet = new Tweet({
            creator: user_id, timeCreated: new Date(), body: tweets[i].body,
            hashtags: getHashtagArray(tweets[i].body)
        });
        tweet.save(tweet, function (err, newTweet) {
            if (err) {
                callback(err);
            }
        });
    }
    callback(null);
}

function getHashtagArray(str) {
    let arr = str.split(' ');
    let output = [];
    for (let item of arr) {
        if (item.startsWith('#')) {
            output.push(item.replace('#', ''));
        }
    }
    return output
}