"use strict";
const mongoose = require("mongoose");
const User = mongoose.model("User");
const Tweet = mongoose.model("Tweet");
const Description = mongoose.model("Description");
const jwt = require('jsonwebtoken');
const request = require('request');
var bcrypt = require('bcrypt');
var url  = require('url');
const saltRounds = 10;

function hash(password, cb) {
    bcrypt.genSalt(saltRounds, function (err, salt) {
        bcrypt.hash(password, salt, function (err, hash) {
            cb(hash)
        });
    });
}

function bcryptVerify(hash, password, cb) {
    bcrypt.compare(password, hash, function (err, res) {
        cb(res)
    });
}


function unfollow(userId, followerId, cb) {
    User.updateOne({_id: userId}, {$pullAll: {followers: [followerId]}}, function (err, user) {
        if (err) {
            cb(err)
        }
        else {
            User.updateOne({_id: followerId}, {$pullAll: {following: [userId]}}, function (err, follower) {
                if (err) {
                    cb(err)
                }
                else {
                    cb(null, user, follower)
                }
            });
        }
    });
}

module.exports.getUser = function (req, res) {
    //console.log("to je id: ", );
    User.findOne({_id: req.payload._id})
        .populate('description', ['body', 'timeCreated'])
        .exec(function (err, data) {
            if (err) {
                res.status(500).json({error: err});
            } else if (!data) {
                res.status(404).json({error: "User not found"});
            } else {
                delete data.password;
                res.status(200).json(data);
            }
        });
};

module.exports.getUserByUserName = function (req, res) {

    User.findOne({username: req.params.username})
        .populate('description', ['body', 'timeCreated'])
        .exec(function (err, data) {
            if (err) {
                res.status(500).json({error: err});
            } else if (!data) {
                res.status(404).json({error: "User not found"});
            } else {
                delete data.password;
                res.status(200).json(data);
            }
        });
}

function IsJsonString(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}

// todo hashanje passworda? za LP4 ne rabimo, sam kasneje pa
module.exports.createUser = function (req, res) {
    //console.log(req.body.recaptch);
    var verificationUrl = "https://www.google.com/recaptcha/api/siteverify?secret=6LeZSz8UAAAAAGLqTwISTuJCdQZ6aF2fC-WuRBez&response=" + req.body.recaptch + "&remoteip=" + req.connection.remoteAddress;
    request(verificationUrl, function (error, response, body) {
        if(!body || error){
          res.status(400).json({error: error});
          return;
        }
        else if(!IsJsonString(body)){
          res.status(400).json({error: "you are robot!!"});
          return;
        }else{
          var body = JSON.parse(body);
          // Success will be true or false depending upon captcha validation.
          hash(req.body.password, function (pass_hash) {
                  req.body.password = pass_hash;

                  if (body.success) {
                      console.log("captch success", body);
                      //captcha successfully verified now you can do next task
                      User.findOne({username: req.body.username}).exec(function (err, _user) {
                          if (_user) {
                              res.status(400).json({error: "User already exists"});
                          }
                          else {
                              //console.log("pridem not!!!");
                              let user = new User(req.body);
                              let description = new Description({body: "Default description", timeCreated: new Date()});
                              description.save(function (err, resD) {
                                  //console.log("na novo shranjeujem!!!!", user, " \n\n", description, " ");
                                  if (err) {
                                      res.status(400).json({error: err});
                                  }
                                  else {
                                      user.description = resD._id;
                                      user.timeJoined = new Date();
                                      //user.nastaviGeslo(req.body.password);
                                      //console.log("posodabljam uporabnika: ", user, "\n\n")
                                      user.save(user, function (err, user) {
                                          if (err) {
                                              //console.log("shranjevanje error")
                                              res.status(400).json({error: err});
                                          }
                                          else {
                                              let jwt_token = generirajJwt(user._id);
                                              res.status(200).json(jwt_token);
                                          }
                                      })
                                  }
                              });

                          }
                      })

                  } else {
                      //captcha not verified send error response to frontend
                      res.status(400).json({error: "robot detected"});
                  }
              }
          );
        }



    });


};

module.exports.follow = function (req, res) {
    let userId = escape(req.body.userId);
    let followerId = req.payload._id;
    if(!userId || !followerId || !typeof  followerId instanceof String || userId.includes('<script>') || userId.includes('</script>') || (userId.match(/%/g) || []).length > 4){
        res.status(404).json({error: "invalid arguments"});
        return;
    }
    User.updateOne({_id: userId}, {$addToSet: {followers: followerId}}, function (err, user) {
        if (!user) {
            res.status(404).json({error: "user not found"});
            return;
        }
        if (err) {
            res.status(500).json({error: err});
            return;
        }
        User.updateOne({_id: followerId}, {$addToSet: {following: userId}}, function (err, follower) {
            if (err || !follower) {
                if (err) {
                    res.status(500).json({error: err});
                }
                else if (!follower) {
                    res.status(404).json({error: "follower not found"});
                }
                unfollow(userId, followerId, function (err, user, follower) {
                    console.log("unfollow", arguments);
                })
            }
            //more useful response?
            res.status(200).json({ok: true})
        })
    })
};

module.exports.unfollow = function (req, res) {
    let userId = escape(req.body.userId);
    if(!req.payload._id || !userId || !typeof  userId instanceof String || userId.includes('<script>') || userId.includes('</script>') || (userId.match(/%/g) || []).length > 4){
        res.status(404).json({error: "missing argumnets"});
        return;
    }
    unfollow(userId, req.payload._id, function (err, user, follower) {
        if (err) {
            res.status(500).json({error: err})
        }
        else {
            //this response should be changed
            res.status(200).json({user, follower})
        }
    });
};

module.exports.deleteUser = function (req, res) {
    let user_id = req.payload._id;
    console.log("poklicana funkcija.......");
    if (!user_id) {
        res.status(404).json({error: "userId missing"});
        return;
    }
    User
        .findByIdAndRemove(user_id)
        .exec(
            function (err, user) {
                if (err) {
                    res.status(500).json(err);
                    return;
                }
                else if (!user) {
                    res.status(404).json({error: "can't find user"});
                }
                else {
                    Description.findByIdAndRemove(user.description).exec(function (err, description) {
                        if (err) {
                            res.status(500).json(err);
                        }
                        else if (!description) {
                            res.status(404).json({error: "can't find user description"});
                        }
                        else {
                            Tweet.remove({creator: user_id}, function (err, tweets) {
                                if (err) {
                                    res.status(500).json(err);
                                    return;
                                }
                                console.log("odstrajnujem2");
                                User.update({followers: {$all: [user_id]}}, {$pullAll: {followers: [user_id]}}, function (err, user) {
                                    if (err) {
                                        res.status(500).json(err);
                                        return;
                                    }
                                    console.log("posodabljam1");
                                    Tweet.update({likes: {$all: [user_id]}}, {$pullAll: {likes: [user_id]}}, function (err, tweet) {
                                        if (err) {
                                            res.status(500).json(err);
                                            return;
                                        }
                                        console.log("posodabljam2");
                                        Tweet.find().exec(function (err, tweets) {
                                            if (err) {
                                                res.status(500).json(err)
                                            } else if (!tweets) {
                                                res.status(404).json({error: "can't find tweets"})
                                            } else {
                                                //console.log(tweets);
                                                for (let i = 0; i < tweets.length; i++) {
                                                    let all_retweets = tweets[i].retweets;

                                                    for (let j = 0; j < all_retweets.length; j++) {

                                                        if (String(all_retweets[j].userId) === user_id) {
                                                            all_retweets.splice(j, 1); //odstrani en retweed, ki ga najdes od uporabnika....
                                                            console.log("brisem");
                                                            break;
                                                        }
                                                    }
                                                    tweets[i].save(function (err, ok) {
                                                        if (err) {
                                                            res.status(500).json(err)
                                                        }
                                                    });
                                                }
                                                res.status(204).json({ok: "ok"})
                                            }
                                        });
                                    });
                                });
                            })
                        }
                    });
                }
            }
        );

};

module.exports.loginCheck = function (req, res) {
    var urls = escape(url.parse(req.url).path);
    let userName = req.body.username;
    let password = req.body.password;
    //console.log(userName + "  " + password);
    if (!userName || !password) {
        res.status(404).json({error: "login parameters missing"});
        return;
    }
    //console.log("notri");
    //console.log(userName, "  and: ", password)
    User.findOne({username: userName}).exec(function (err, user) {
        if (err) {
            res.status(500).json({error: err});
        }
        else if (!user) {
            res.status(404).json({error: "User not found"});
        }
        else {

            //vse ok, overjanje uspeno vrnemo uporabnika vn, da si ga potem lahko v clinet strani da je logiran. POmemben je bolj kot ne njegov id.
            bcryptVerify(user.password, password, function (isSameHash) {
                if (isSameHash) {
                    let jwt_token = generirajJwt(user._id);

                    res.status(200).json(jwt_token);
                } else {
                    res.status(401).json({error: "incorrect password"})
                }
            })


        }
    })
};

function generirajJwt(id) {
    var datumPoteka = new Date();
    datumPoteka.setDate(datumPoteka.getDate() + 7);
    console.log("process.env.JWT_GESLO");
    console.log(process.env.JWT_GESLO);
    return jwt.sign({
        _id: id,
        datumPoteka: parseInt(datumPoteka.getTime() / 1000, 10)
    }, process.env.JWT_GESLO);
};

module.exports.changePassword = function (req, res) {
    let newPassword = req.body.newPassword;
    let user_id = req.payload._id;
    let oldPassword = req.body.oldPassword;
    //console.log("klic");
    if (!newPassword || !user_id || !oldPassword) {
        //console.log("notr!!!!!!!!!!!!!!!!!!!");
        res.status(404).json({error: "parameters missing"})

        return;
    }

    User.findOne({_id: user_id}).exec(function (err, user) {
        //console.log(user.password)
        //console.log(oldPassword)
        //console.log(newPassword)
        if (err) {
            console.log("prvi");
            res.status(500).json({error: err});
        }
        else if (!user) {
            res.status(404).json({error: "user not found"});
        }
        else {
            //console.log(user.password, " OLD ", oldPassword);
            bcryptVerify(user.password, oldPassword, function (isSameHash) {
                if (isSameHash) {
                    //console.log(user);
                    hash(newPassword, function (pass_hash) {
                        user.password = pass_hash;
                        console.log(user.password)
                        user.save(function (err, user) {
                            if (err) {
                                console.log(err);
                                //console.log(user);
                                res.status(500).json({error: err})
                            }
                            else {
                                //mogoce je potreben drugacen odziv?????????????????????????????????????????????
                                res.status(200).json(user);
                            }
                        });
                    })

                } else {
                    res.status(404).json({error: "passwords do no match"})
                }
            })


        }
    })
};

module.exports.changeDescription = function (req, res) {
    let newDescription = req.body.newDescription;
    let user_id = req.payload._id;
    if (!newDescription) {
        res.status(404).json({error: "missing description"});
        return;
    }
    else if (!user_id) {
        res.status(404).json({error: "missing userId"});
        return;
    } else {
        User.findOne({_id: user_id}).exec(function (err, user) {
            if (err) {
                res.json(500).json({error: err});
            }
            else if (!user) {
                res.json(404).json({error: "user not found"});
            }
            else {
                let newData = {body: newDescription, timeCreated: new Date()};
                Description.findOneAndUpdate({_id: user.description}, newData, {upsert: true}, function (err, description) {
                    if (err) {
                        res.status(500).json({error: err});
                    }
                    else {
                        res.status(200).json(description);
                    }
                });
            }
        })
    }

};

module.exports.getRecommendations = function (req, res) {
    let user_id = req.payload._id;
    if (!user_id) {
        res.status(404).json({error: "no user given."});
        return;
    }
    User.find({followers: {$nin: [user_id]}}).populate('description', ['body', 'timeCreated']).exec(function (err, users) {
        if (err) {
            res.status(500).json(err);
        } else {
            res.status(200).json(users);
        }
    });
}

module.exports.getAllFollowing = function (req, res) {
    let user_id = req.payload._id;
    if (!user_id) {
        res.status(404).json({error: "no user given"});
    } else {
        User.find({followers: {$in: [user_id]}}).populate('description', ['body', 'timeCreated']).exec(function (err, users) {
            if (err) {
                res.status(500).json(err);
            } else {
                res.status(200).json(users);
            }
        });
    }
}
