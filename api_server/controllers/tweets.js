"use strict";
const mongoose = require("mongoose");
const Tweet = mongoose.model("Tweet");
const User = mongoose.model("User");

function getHashtagArray(str) {
    let arr = str.split(" ");
    let output = [];
    for (let item of arr) {
        if (item.startsWith('#')) {
            let index = item.indexOf("\n");
            if (index >= 0) {
                item = item.substring(0, index);

            }
            output.push(item.replace('#', ''));
        }
    }
    let arr2 = str.split("\n");
    for (let item of arr2) {
        if (item.startsWith('#')) {
            output.push(item.replace('#', ''));
        }
    }
    return output
}

module.exports.getTweet = function (req, res) {
    Tweet.findOne({_id: req.params.tweetId}).populate('creator', 'username').exec(function (err, data) {
        if (err) {
            res.status(500).json({error: err});
        } else if (!data) {
            res.status(404).json({error: "Tweet not found"});
        } else {
            res.status(200).json(data);
        }
    });
};

module.exports.createTweet = function (req, res) {
  if(!req.payload._id || !req.body.body || !typeof  req.body.body instanceof String || req.body.body.includes('<script>') || req.body.body.includes('</script>')){
      res.status(404).json({error: "invalid arguments"});
      return;
  }

    User.findOne({_id: req.payload._id}).exec(function (err, user) {
        if (err) {
            res.status(500).json({error: err});
        } else if (!user) {
            res.status(404).json({error: "Creator not found"});
        } else {
            let tweet = req.body;

            tweet.creator = user._id;
            tweet.hashtags = getHashtagArray(tweet.body);
            tweet.timeCreated = new Date();

            Tweet.create(tweet, function (err, tweet) {
                if (err) {
                    res.status(400).json({error: err});
                }
                else {
                    res.status(200).json(tweet);
                }
            })
        }
    });
};

module.exports.retweet = function (req, res) {
    let tweetId = req.params.tweetId,
        userId = req.payload._id;
    User.findOne({_id: userId}).exec(function (err, user) {
        if (err) {
            res.status(500).json({error: err});
        } else if (!user) {
            res.status(404).json({error: "Creator not found"});
        } else {
            console.log(user._id);
            Tweet.updateOne({_id: tweetId}, {
                $addToSet: {
                    retweets: {
                        userId: user._id,
                        timeSent: Date.now()
                    }
                }
            }).exec(function (err, tweet) {
                if (err) {
                    res.status(500).json({error: err});
                } else if (!user) {
                    res.status(404).json({error: "Tweet not found"});
                } else {
                    console.log(tweet);
                    res.status(200).json({ok: "ok"});
                }
            });
        }
    });
};


module.exports.deleteTweet = function (req, res) {
    let tweetId = req.params.tweetId; //pridobi tweet id od zahteve.
    if (!tweetId) {
        //ce ni tweetID, vrni napako...
        res.status(404).json({error: "no tweet for delation"});
        return;
    }
    Tweet
        .findByIdAndRemove(tweetId)
        .exec(
            function (err, tweet) {
                if (err) {
                    res.status(404).json(err);
                    return;
                }
                res.status(204).json(null); //204 za uspešno brisanje....
            }
        );

}


module.exports.getFeed = function (req, res) {
    User.findOne({_id: req.payload._id}).exec(function (err, user) {
        if (err) {
            res.status(500).json({error: err})
        }
        else if (!user) {
            res.status(404).json({error: "User not found"})
        }
        else {
            Tweet.find({
                $or: [
                    {
                        creator: user._id
                    },
                    {
                        creator: {
                            $in: user.following //creator mora bit v user. following
                        }

                    },
                    {
                        "retweets.userId": {
                            $in: user.following
                        }

                    }

                ]
            })
                .sort({
                    timeCreated: -1
                })
                .populate('creator', 'username')
                .limit(50)
                .exec(function (err, tweets) {
                    if (err) {
                        res.status(500).json({error: err});
                    }
                    else {
                        res.status(200).json(tweets);
                    }
                })
        }
    })
};

module.exports.search = async function (req, res) {

    let {hashtags, usernames} = req.query,
        hashtagArr = hashtags ? hashtags.split(',') : [],
        usernameArr = usernames ? usernames.split(',') : [];
    //console.log("usernames: ", usernameArr, "hash: ", hashtagArr);
    try {
        let userIds = await User.find({username: {$in: usernameArr}}).select({_id: 1});
        let tweets = await Tweet.find({
            $or: [
                {
                    hashtags: {
                        $in: hashtagArr
                    }
                },
                {
                    creator: {
                        $in: userIds
                    }
                }
            ]
        }).populate('creator', 'username');
        res.status(200).json(tweets);
    }
    catch (e) {
        res.status(500).json({error: e})
    }
};


module.exports.addLike = function (req, res) {
    let tweetId = req.params.tweetId,
        userId = req.payload._id;

    User.findOne({_id: userId}).exec(function (err, user) {
        if (err) {
            res.status(500).json({error: err});
        } else if (!user) {
            console.log("oj vey....")
            res.status(404).json({error: "User not found"});
        } else {
            Tweet.updateOne({_id: tweetId}, {$addToSet: {likes: userId}}, function (err, status) {
                if (err) {
                    res.status(500).json({error: err});
                    return;
                }
                else if (status.n === 0) {
                    res.status(404).json({error: "tweet not found"});
                    return;
                }
                else if (status.nModified === 0) {
                    res.status(404).json({error: "You can only Like a tweet once"});
                    return;
                }
                //more useful response?
                res.status(200).json({ok: true})
            })
        }
    });

};

module.exports.removeLike = function (req, res) {
    let tweetId = req.params.tweetId,
        userId = req.payload._id

    User.findOne({_id: userId}).exec(function (err, user) {
        if (err) {
            res.status(500).json({error: err});
        } else if (!user) {
            res.status(404).json({error: "User not found"});
        } else {
            Tweet.updateOne({_id: tweetId}, {$pullAll: {likes: [userId]}}, function (err, status) {
                if (err) {
                    res.status(500).json({error: err});
                    return;
                }
                else if (status.n === 0) {
                    res.status(404).json({error: "tweet not found"});
                    return;
                }
                else if (status.nModified === 0) {
                    res.status(404).json({error: "Sorry, you can only un-like tweets, not dislike them. We don't want to trigger anyone."});
                    return;
                }
                //more useful response?
                res.status(200).json({ok: true})
            })
        }
    });

};
