const router = require("express").Router(),
    //returns mainly tweet(s)
    ctrlTweets = require("../controllers/tweets"),
    //returns mainly user(s)
    ctrlUsers = require("../controllers/users"),
    //kontrole za init ali izbris DB
    ctrlDb = require("../controllers/dbControll");

const jwt = require('express-jwt');

//to je middleware
var avtentikacija = jwt({
    secret: process.env.JWT_GESLO, //geslo, ki je shranjeno na strani streznika...
    userProperty: 'payload' //tukaj se nahaja koristna vsebina...
});


// Tweets
//router.get("/tweet/:tweetId", ctrlTweets.getTweet);
router.put("/tweet", avtentikacija, ctrlTweets.createTweet);

router.delete("/tweet/:tweetId", avtentikacija, ctrlTweets.deleteTweet);
router.post("/tweet/:tweetId/like", avtentikacija, ctrlTweets.addLike);
router.post("/tweet/:tweetId/unlike", avtentikacija, ctrlTweets.removeLike);
router.post("/tweet/:tweetId/retweet", avtentikacija, ctrlTweets.retweet);


// Multiple tweets
router.get("/feed", avtentikacija, ctrlTweets.getFeed);
router.get("/search", ctrlTweets.search);

// Users
router.get("/user", avtentikacija, ctrlUsers.getUser);
router.put("/user", ctrlUsers.createUser);
router.post("/user/follow", avtentikacija, ctrlUsers.follow);
router.post("/user/unfollow", avtentikacija, ctrlUsers.unfollow);
router.delete("/user", avtentikacija, ctrlUsers.deleteUser);
router.post("/user/login", ctrlUsers.loginCheck);
router.post("/user/changePassword", avtentikacija, ctrlUsers.changePassword);
router.post("/user/changeDescription", avtentikacija, ctrlUsers.changeDescription);

router.get("/user/recommend", avtentikacija, ctrlUsers.getRecommendations);
router.get("/user/following", avtentikacija, ctrlUsers.getAllFollowing);
router.get("/user/username/:username", ctrlUsers.getUserByUserName);

// Other --> zahtevki iz ./db dela, kjer je zahtevan ibris ali init databaze.
router.delete("/db/delete", ctrlDb.deleteAll);
router.put("/db/init", ctrlDb.init);

module.exports = router;
