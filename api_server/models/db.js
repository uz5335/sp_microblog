//TODO: RIP slovenscina, ES6
var mongoose = require("mongoose");
var dbURI = "mongodb://localhost/microblog"; //---->lokalna povezava

//var dbURI = "mongodb://user:pass@ds129386.mlab.com:29386/mikroblog"
// console.log("is it prod mode?");

if (process.env.NODE_ENV === "production") {
    console.log("prod mode");
    dbURI = process.env.MLAB_URI;
}
// dbURI = "mongodb://user:pass@ds129386.mlab.com:29386/mikroblog";

mongoose.connect(dbURI, { useMongoClient: true });

mongoose.connection.on("connected", function() {
    console.log("Mongoose je povezan na " + dbURI);
});

mongoose.connection.on("error", function(err) {
    console.log("Mongoose connection error: " + err);
});
mongoose.connection.on("disconnected", function() {
    console.log("Mongoose je zaprl povezavo");
});

var pravilnaUstavitev = function(sporocilo, povratniKlic) {
    mongoose.connection.close(function() {
        console.log("Mongoose je zaprl povezavo preko " + sporocilo);
        povratniKlic();
    });
};

// Pri ponovnem zagonu nodemon
process.once("SIGUSR2", function() {
    pravilnaUstavitev("nodemon ponovni zagon", function() {
        process.kill(process.pid, "SIGUSR2");
    });
});

// Pri izhodu iz aplikacije
process.on("SIGINT", function() {
    pravilnaUstavitev("izhod iz aplikacije", function() {
        process.exit(0);
    });
});

// Pri izhodu iz aplikacije na Heroku
process.on("SIGTERM", function() {
    pravilnaUstavitev("izhod iz aplikacije na Heroku", function() {
        process.exit(0);
    });
});

require("./schemas");
