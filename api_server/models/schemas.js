const mongoose = require("mongoose"),
    Schema = mongoose.Schema;

mongoose.Promise = global.Promise;

const descriptionSchema = new Schema({
    body: {
        required: true,
        type: String
    },
    timeCreated: {
        required: true,
        type: Date
    }
}, {
    versionKey: false
});

const userShema = new Schema({
        username: {
            type: String,
            required: true,
            maxLength: 20,
            minLength: 4
        },
        password: {
            type: String,
            //required: true
        },

        description: {
            type: Schema.ObjectId,
            ref: "Description",
            required: true
        },
        timeJoined: {
            required: true,
            type: Date
        },
        following: {
            type: [
                {
                    type: Schema.ObjectId,
                    ref: 'User'
                }
            ]
        },
        followers: {
            type: [
                {
                    type: Schema.ObjectId,
                    ref: 'User'
                }
            ]
        }
    },
    {
        versionKey: false
    });

/* userShema.methods.nastaviGeslo = function(geslo) {  
    this.password = bcrypt.hashSync(geslo, saltRounds);
 };
 
 userShema.methods.preveriGeslo = function(geslo) {  
    return bcrypt.compareSync(geslo, this.password);
 };*/


const retweetSchema = new Schema({
        userId: {
            type: Schema.ObjectId,
            required: true,
            ref: "User"
        },
        timeSent: {
            type: Date,
            required: true,
            index: true
        }
    },
    {
        versionKey: false
    });

const tweetSchema = new Schema({
        creator: {
            type: Schema.ObjectId,
            required: true,
            ref: "User"
        },
        timeCreated: {
            type: Date,
            required: true,
            index: true
        },
        body: {
            type: String,
            required: true,
            minLength: 1,
            maxLength: 250
        },
        hashtags: {
            type: [String],
        },
        likes: {
            type: [{
                type: Schema.ObjectId,
                ref: "User"
            }],
        },
        retweets: {
            type: [retweetSchema],
        }
    },
    {
        versionKey: false
    });
/*
const likeSchema = new Schema({
    userId: {
        required: true,
        type: Schema.ObjectId,
        ref: 'User'
    },
    tweetId: {
        required: true,
        type: Schema.ObjectId,
        ref: 'Tweet'
    }
}, {
    versionKey: false
});
*/


mongoose.model("User", userShema);
mongoose.model("Tweet", tweetSchema);
//mongoose.model("Like", likeSchema);
mongoose.model("Description", descriptionSchema);

console.log('schemas created');
