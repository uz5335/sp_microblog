var express = require("express");
var path = require("path");
var favicon = require("serve-favicon");
var logger = require("morgan");
var cookieParser = require("cookie-parser");
var bodyParser = require("body-parser");
var fs = require('fs');
var url  = require('url');
var dateFormat = require('dateformat');
var uglifyJs = require('uglify-es');
var helmet = require('helmet')

require('./api_server/models/db');
var api = require("./api_server/routes/index");
//var index = require("./app_server/routes/index");
var db = require('./app_server/routes/set_db');
var noScript = require('./app_server/routes/noScript');

var app = express();

app.use(helmet());
app.use(function(req, res, next) {
  res.setHeader('X-Frame-Options', 'DENY');
  res.setHeader('X-XSS-Protection', '1; mode=block');
  res.setHeader('X-Content-Type-Options', 'nosniff');
  next();
});
// view engine setup
app.set("views", path.join(__dirname, "app_server", "views"));
app.set("view engine", "jade");

var zdruzeno = uglifyJs.minify({
    'app.js': fs.readFileSync('app_client/app.js', 'utf8'),
    'navbar.js': fs.readFileSync('app_client/directives/navbar/navbar.js', 'utf8'),
    'header.js': fs.readFileSync('app_client/directives/loginAndRegisterHeader/header.js', 'utf8'),
    'registerCtrl.js': fs.readFileSync('app_client/register/registerCtrl.js', 'utf8'),
    'api.js': fs.readFileSync('app_client/services/api.js', 'utf8'),
    'loginCtrl.js': fs.readFileSync('app_client/login/loginCtrl.js', 'utf8'),
    'authentication.js': fs.readFileSync('app_client/services/authentication.js', 'utf8'),
    'homeCtrl.js': fs.readFileSync('app_client/main/home/homeCtrl.js', 'utf8'),
    'profileCtrl.js': fs.readFileSync('app_client/main/profile/profileCtrl.js', 'utf8'),
    'followers.js': fs.readFileSync('app_client/main/followers/followers.js', 'utf8'),
    'searchCtrl.js': fs.readFileSync('app_client/directives/search/searchCtrl.js', 'utf8'),
    'dropdown.js': fs.readFileSync('app_client/directives/dropdownMenu/dropdown.js', 'utf8'),
    'dropdownCtrl.js': fs.readFileSync('app_client/directives/dropdownMenu/dropdownCtrl.js', 'utf8'),
    'userInfo.js': fs.readFileSync('app_client/directives/userInfo/userInfo.js', 'utf8'),
    'userInfoCtrl.js': fs.readFileSync('app_client/directives/userInfo/userInfoCtrl.js', 'utf8'),
    'tweetcard.js': fs.readFileSync('app_client/directives/tweetcard/tweetcard.js', 'utf8'),
    'tweetcardCtrl.js': fs.readFileSync('app_client/directives/tweetcard/tweetcardCtrl.js', 'utf8'),
    'search.js': fs.readFileSync('app_client/directives/search/search.js', 'utf8'),
    'searchCtrl1.js': fs.readFileSync('app_client/main/search/searchCtrl1.js', 'utf8'),
});

fs.writeFile('public/angular/microblog.min.js', zdruzeno.code, function(napaka) {
    if (napaka)
        console.log(napaka);
    else
        console.log('Skripta je zgenerirana in shranjena v "edugeocache.min.js"');
});


// function logResponseBody(req, res, next) {
//     var urls = url.parse(req.url)
//     if(urls.path.includes('api')){
//         var stream = fs.createWriteStream(path.join(__dirname, 'apiLogs.log'), {flags: 'a'});
//         stream.once('open', function(fd) {
//             stream.write(String(req.method+" "+urls.path+" "+dateFormat(new Date(),"dddd, mmmm dS, yyyy, h:MM:ss TT"))+"\n");
//             stream.end();
//         });
//         //console.log(urls);
//     }
//   next();
// }

//app.use(logResponseBody);
app.use(logger('dev'));
// app.use(logger('common', {
//   stream: fs.createWriteStream(path.join(__dirname, 'error.log'), {flags: 'a'}),
//   skip: function (req, res) { return res.statusCode < 400 }
// }))
/*
app.use(logger('combined', {
  stream: fs.createWriteStream(path.join(__dirname, 'access.log'), {flags: 'a'}),
}))*/

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));
app.use(favicon(path.join(__dirname, "public", "images", "favicon.ico")));
app.use(express.static(path.join(__dirname, 'app_client')));

//app.use("/", index); //za angularJS del se mora zakomentirat..
app.use("/api", require('./logger'));
app.use("/api", api);
app.use('/db', db);
app.use('/noScript', noScript);

app.use(function (req, res) {
    res.sendFile(path.join(__dirname, 'app_client', 'index.html')); //poslji datoteko index.html.
});

app.use(function (err, req, res, next) {
    //ce gre za tip napeke...
    if (err.name === 'UnauthorizedError') {
        res.status(401);
        res.json({"sporočilo": err.name + ": " + err.message});
    }
});

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error("Not Found");
    err.status = 404;
    next(err);
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get("env") === "development" ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render("error");
});

module.exports = app;
