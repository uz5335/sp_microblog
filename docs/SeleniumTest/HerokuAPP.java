package com.example.tests;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class HerokuAPP {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @Before
  public void setUp() throws Exception {
    driver = new FirefoxDriver();
    baseUrl = "https://www.katalon.com/";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void testHerokuAPP() throws Exception {
    driver.findElement(By.id("login-username")).click();
    driver.findElement(By.id("login-username")).clear();
    driver.findElement(By.id("login-username")).sendKeys("user");
    driver.findElement(By.id("login-password")).click();
    driver.findElement(By.id("login-password")).clear();
    driver.findElement(By.id("login-password")).sendKeys("pass");
    driver.findElement(By.id("btn-login")).click();
    driver.findElement(By.id("filter")).click();
    driver.findElement(By.cssSelector("html.ng-scope")).click();
    driver.findElement(By.id("textarea")).click();
    driver.findElement(By.id("textarea")).clear();
    driver.findElement(By.id("textarea")).sendKeys("nov tweet...#lol");
    driver.findElement(By.id("btn-new-tweet")).click();
    driver.findElement(By.id("search")).click();
    driver.findElement(By.id("search")).clear();
    driver.findElement(By.id("search")).sendKeys("#lol");
    driver.findElement(By.id("search")).sendKeys(Keys.ENTER);
    driver.findElement(By.cssSelector("img[alt=\"logo\"]")).click();
    // ERROR: Caught exception [ERROR: Unsupported command [mouseOver | css=img[alt="logo"] | ]]
    driver.findElement(By.id("search")).click();
    driver.findElement(By.id("search")).clear();
    driver.findElement(By.id("search")).sendKeys("#lol");
    driver.findElement(By.id("search")).sendKeys(Keys.ENTER);
    driver.findElement(By.cssSelector("img[alt=\"logo\"]")).click();
    // ERROR: Caught exception [ERROR: Unsupported command [mouseOver | css=img[alt="logo"] | ]]
    driver.findElement(By.id("textarea")).click();
    driver.findElement(By.id("textarea")).clear();
    driver.findElement(By.id("textarea")).sendKeys("#lol");
    driver.findElement(By.id("btn-new-tweet")).click();
    driver.findElement(By.id("search")).click();
    driver.findElement(By.id("search")).clear();
    driver.findElement(By.id("search")).sendKeys("#lol");
    driver.findElement(By.id("search")).sendKeys(Keys.ENTER);
    driver.findElement(By.cssSelector("a.glyphicon.glyphicon-trash.ng-scope")).click();
    driver.findElement(By.cssSelector("button.tingle-btn.tingle-btn--pull-right.tingle-btn--danger")).click();
    driver.findElement(By.cssSelector("img[alt=\"logo\"]")).click();
    // ERROR: Caught exception [ERROR: Unsupported command [mouseOver | css=img[alt="logo"] | ]]
    driver.findElement(By.xpath("//tweetcard[2]/div/div[2]/a[2]")).click();
    driver.findElement(By.cssSelector("button.tingle-btn.tingle-btn--pull-right.tingle-btn--danger")).click();
    driver.findElement(By.id("dropdownMenuButton")).click();
    driver.findElement(By.linkText("Following")).click();
    driver.findElement(By.cssSelector("div.col-xs-9.text-left > h3.activeUser-name.ng-binding")).click();
    driver.findElement(By.cssSelector("img[alt=\"logo\"]")).click();
    // ERROR: Caught exception [ERROR: Unsupported command [mouseOver | css=img[alt="logo"] | ]]
    driver.findElement(By.id("dropdown-style")).click();
    driver.findElement(By.id("dropdown-style")).click();
    driver.findElement(By.id("dropdownMenuButton")).click();
    driver.findElement(By.linkText("Profile")).click();
    driver.findElement(By.id("textarea1")).click();
    driver.findElement(By.id("textarea1")).clear();
    driver.findElement(By.id("textarea1")).sendKeys("nov opis");
    driver.findElement(By.id("btn-change-description")).click();
    driver.findElement(By.id("password")).click();
    driver.findElement(By.id("password")).clear();
    driver.findElement(By.id("password")).sendKeys("1");
    driver.findElement(By.id("password1")).click();
    driver.findElement(By.id("password1")).clear();
    driver.findElement(By.id("password1")).sendKeys("d");
    driver.findElement(By.id("password2")).click();
    driver.findElement(By.id("password2")).clear();
    driver.findElement(By.id("password2")).sendKeys("d");
    driver.findElement(By.id("btn-change-password")).click();
    driver.findElement(By.id("password")).clear();
    driver.findElement(By.id("password")).sendKeys("");
    driver.findElement(By.id("password1")).clear();
    driver.findElement(By.id("password1")).sendKeys("");
    driver.findElement(By.id("password2")).clear();
    driver.findElement(By.id("password2")).sendKeys("");
    driver.findElement(By.id("password")).click();
    driver.findElement(By.id("password")).clear();
    driver.findElement(By.id("password")).sendKeys("pass");
    driver.findElement(By.id("password1")).clear();
    driver.findElement(By.id("password1")).sendKeys("123");
    driver.findElement(By.id("password2")).click();
    driver.findElement(By.id("password2")).clear();
    driver.findElement(By.id("password2")).sendKeys("123");
    driver.findElement(By.xpath("//div[4]")).click();
    driver.findElement(By.id("btn-change-password")).click();
    driver.findElement(By.id("dropdownMenuButton")).click();
    driver.findElement(By.id("logOut")).click();
    driver.findElement(By.cssSelector("button.tingle-btn.tingle-btn--pull-right.tingle-btn--danger")).click();
    driver.findElement(By.id("login-username")).click();
    driver.findElement(By.id("login-username")).clear();
    driver.findElement(By.id("login-username")).sendKeys("user");
    driver.findElement(By.id("login-password")).click();
    driver.findElement(By.id("login-password")).clear();
    driver.findElement(By.id("login-password")).sendKeys("pass");
    driver.findElement(By.id("btn-login")).click();
    driver.findElement(By.id("login-username")).click();
    driver.findElement(By.id("login-username")).click();
    driver.findElement(By.id("login-username")).clear();
    driver.findElement(By.id("login-username")).sendKeys("");
    driver.findElement(By.id("login-password")).click();
    driver.findElement(By.id("login-password")).click();
    driver.findElement(By.id("login-password")).clear();
    driver.findElement(By.id("login-password")).sendKeys("");
    driver.findElement(By.id("login-username")).click();
    driver.findElement(By.id("login-username")).click();
    driver.findElement(By.id("login-username")).clear();
    driver.findElement(By.id("login-username")).sendKeys("user");
    driver.findElement(By.id("login-password")).click();
    driver.findElement(By.id("login-password")).clear();
    driver.findElement(By.id("login-password")).sendKeys("pass");
    driver.findElement(By.id("btn-login")).click();
    driver.findElement(By.id("login-password")).click();
    driver.findElement(By.id("login-password")).clear();
    driver.findElement(By.id("login-password")).sendKeys("");
    driver.findElement(By.id("login-username")).click();
    driver.findElement(By.id("login-username")).clear();
    driver.findElement(By.id("login-username")).sendKeys("");
    driver.findElement(By.id("login-password")).click();
    driver.findElement(By.id("login-username")).click();
    driver.findElement(By.id("login-username")).clear();
    driver.findElement(By.id("login-username")).sendKeys("user");
    driver.findElement(By.id("login-password")).click();
    driver.findElement(By.id("login-password")).click();
    driver.findElement(By.id("login-password")).clear();
    driver.findElement(By.id("login-password")).sendKeys("123");
    driver.findElement(By.id("btn-login")).click();
    driver.findElement(By.id("dropdownMenuButton")).click();
    driver.findElement(By.linkText("Profile")).click();
    // ERROR: Caught exception [ERROR: Unsupported command [mouseOver | link=Profile | ]]
    driver.findElement(By.id("delete-profile")).click();
    driver.findElement(By.cssSelector("button.tingle-btn.tingle-btn--pull-right.tingle-btn--danger")).click();
    // ERROR: Caught exception [ERROR: Unsupported command [mouseOver | link=Register instead | ]]
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
