import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS

import com.thoughtworks.selenium.Selenium
import org.openqa.selenium.firefox.FirefoxDriver
import org.openqa.selenium.WebDriver
import com.thoughtworks.selenium.webdriven.WebDriverBackedSelenium
import static org.junit.Assert.*
import java.util.regex.Pattern
import static org.apache.commons.lang3.StringUtils.join

WebUI.openBrowser('https://www.katalon.com/')
def driver = DriverFactory.getWebDriver()
String baseUrl = "https://www.katalon.com/"
selenium = new WebDriverBackedSelenium(driver, baseUrl)
selenium.click("id=login-username")
selenium.type("id=login-username", "user")
selenium.click("id=login-password")
selenium.type("id=login-password", "pass")
selenium.click("id=btn-login")
selenium.click("id=filter")
selenium.click("css=html.ng-scope")
selenium.click("id=textarea")
selenium.type("id=textarea", "nov tweet...#lol")
selenium.click("id=btn-new-tweet")
selenium.click("id=search")
selenium.type("id=search", "#lol")
selenium.sendKeys("id=search", "${KEY_ENTER}")
selenium.click("css=img[alt=\"logo\"]")
selenium.mouseOver("css=img[alt=\"logo\"]")
selenium.click("id=search")
selenium.type("id=search", "#lol")
selenium.sendKeys("id=search", "${KEY_ENTER}")
selenium.click("css=img[alt=\"logo\"]")
selenium.mouseOver("css=img[alt=\"logo\"]")
selenium.click("id=textarea")
selenium.type("id=textarea", "#lol")
selenium.click("id=btn-new-tweet")
selenium.click("id=search")
selenium.type("id=search", "#lol")
selenium.sendKeys("id=search", "${KEY_ENTER}")
selenium.click("css=a.glyphicon.glyphicon-trash.ng-scope")
selenium.click("css=button.tingle-btn.tingle-btn--pull-right.tingle-btn--danger")
selenium.click("css=img[alt=\"logo\"]")
selenium.mouseOver("css=img[alt=\"logo\"]")
selenium.click("//tweetcard[2]/div/div[2]/a[2]")
selenium.click("css=button.tingle-btn.tingle-btn--pull-right.tingle-btn--danger")
selenium.click("id=dropdownMenuButton")
selenium.click("link=Following")
selenium.click("css=div.col-xs-9.text-left > h3.activeUser-name.ng-binding")
selenium.click("css=img[alt=\"logo\"]")
selenium.mouseOver("css=img[alt=\"logo\"]")
selenium.click("id=dropdown-style")
selenium.click("id=dropdown-style")
selenium.click("id=dropdownMenuButton")
selenium.click("link=Profile")
selenium.click("id=textarea1")
selenium.type("id=textarea1", "nov opis")
selenium.click("id=btn-change-description")
selenium.click("id=password")
selenium.type("id=password", "1")
selenium.click("id=password1")
selenium.type("id=password1", "d")
selenium.click("id=password2")
selenium.type("id=password2", "d")
selenium.click("id=btn-change-password")
selenium.type("id=password", "")
selenium.type("id=password1", "")
selenium.type("id=password2", "")
selenium.click("id=password")
selenium.type("id=password", "pass")
selenium.type("id=password1", "123")
selenium.click("id=password2")
selenium.type("id=password2", "123")
selenium.click("//div[4]")
selenium.click("id=btn-change-password")
selenium.click("id=dropdownMenuButton")
selenium.click("id=logOut")
selenium.click("css=button.tingle-btn.tingle-btn--pull-right.tingle-btn--danger")
selenium.click("id=login-username")
selenium.type("id=login-username", "user")
selenium.click("id=login-password")
selenium.type("id=login-password", "pass")
selenium.click("id=btn-login")
selenium.click("id=login-username")
selenium.click("id=login-username")
selenium.type("id=login-username", "")
selenium.click("id=login-password")
selenium.click("id=login-password")
selenium.type("id=login-password", "")
selenium.click("id=login-username")
selenium.click("id=login-username")
selenium.type("id=login-username", "user")
selenium.click("id=login-password")
selenium.type("id=login-password", "pass")
selenium.click("id=btn-login")
selenium.click("id=login-password")
selenium.type("id=login-password", "")
selenium.click("id=login-username")
selenium.type("id=login-username", "")
selenium.click("id=login-password")
selenium.click("id=login-username")
selenium.type("id=login-username", "user")
selenium.click("id=login-password")
selenium.click("id=login-password")
selenium.type("id=login-password", "123")
selenium.click("id=btn-login")
selenium.click("id=dropdownMenuButton")
selenium.click("link=Profile")
selenium.mouseOver("link=Profile")
selenium.click("id=delete-profile")
selenium.click("css=button.tingle-btn.tingle-btn--pull-right.tingle-btn--danger")
selenium.mouseOver("link=Register instead")
