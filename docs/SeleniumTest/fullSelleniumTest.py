# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re

class UspesnaPrijava(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = "https://www.katalon.com/"
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_uspesna_prijava(self):
        driver = self.driver
        driver.get("http://localhost:3000/")
        driver.find_element_by_id("login-username").click()
        driver.find_element_by_id("login-username").clear()
        driver.find_element_by_id("login-username").send_keys("user")
        driver.find_element_by_id("login-password").clear()
        driver.find_element_by_id("login-password").send_keys("pass")
        driver.find_element_by_id("btn-login").click()
        driver.find_element_by_xpath("//div[2]/a[2]").click()
        driver.find_element_by_xpath("//button[2]").click()
        driver.find_element_by_xpath("//div[@id='goGome']/div[2]").click()
        driver.find_element_by_id("textarea").click()
        driver.find_element_by_id("textarea").clear()
        driver.find_element_by_id("textarea").send_keys("nov tweet")
        driver.find_element_by_id("btn-new-tweet").click()
        driver.find_element_by_id("dropdown-style").click()
        driver.find_element_by_id("dropdownMenuButton").click()
        driver.find_element_by_id("textarea").click()
        driver.find_element_by_id("textarea").clear()
        driver.find_element_by_id("textarea").send_keys("#lol")
        driver.find_element_by_id("btn-new-tweet").click()
        driver.find_element_by_id("search").click()
        driver.find_element_by_id("search").clear()
        driver.find_element_by_id("search").send_keys("#lol")
        driver.find_element_by_id("search").send_keys(Keys.ENTER)
        driver.find_element_by_xpath("//div[2]/a[2]").click()
        driver.find_element_by_xpath("//button[2]").click()
        driver.find_element_by_xpath("//img[@alt='logo']").click()
        driver.find_element_by_id("dropdown-style").click()
        driver.find_element_by_id("dropdownMenuButton").click()
        driver.find_element_by_link_text("Profile").click()
        driver.find_element_by_id("textarea1").click()
        driver.find_element_by_id("textarea1").clear()
        driver.find_element_by_id("textarea1").send_keys("to je nov opis")
        driver.find_element_by_id("btn-change-description").click()
        driver.find_element_by_xpath("//img[@alt='logo']").click()
        driver.find_element_by_xpath("//tweetcard/div/div").click()
        driver.find_element_by_id("search").click()
        driver.find_element_by_id("search").clear()
        driver.find_element_by_id("search").send_keys("@user2")
        driver.find_element_by_id("search").send_keys(Keys.ENTER)
        driver.find_element_by_xpath("//img[@alt='logo']").click()
        driver.find_element_by_id("dropdownMenuButton").click()
        driver.find_element_by_link_text("Home").click()
        driver.find_element_by_id("dropdownMenuButton").click()
        driver.find_element_by_link_text("Following").click()
        driver.find_element_by_xpath("//div[@id='goGome']/div[2]/h3").click()
        driver.find_element_by_id("dropdownMenuButton").click()
        driver.find_element_by_id("dropdownMenuButton").click()
        driver.find_element_by_link_text("Profile").click()
        driver.find_element_by_id("password").click()
        driver.find_element_by_id("password").clear()
        driver.find_element_by_id("password").send_keys("das")
        driver.find_element_by_id("password1").click()
        driver.find_element_by_id("password1").clear()
        driver.find_element_by_id("password1").send_keys("123")
        driver.find_element_by_id("password2").click()
        driver.find_element_by_id("password2").clear()
        driver.find_element_by_id("password2").send_keys("123")
        driver.find_element_by_id("btn-change-password").click()
        driver.find_element_by_id("password").click()
        driver.find_element_by_id("password").clear()
        driver.find_element_by_id("password").send_keys("")
        driver.find_element_by_id("password1").clear()
        driver.find_element_by_id("password1").send_keys("")
        driver.find_element_by_id("password2").clear()
        driver.find_element_by_id("password2").send_keys("")
        driver.find_element_by_xpath("//div[4]").click()
        driver.find_element_by_xpath("//body/div/div").click()
        driver.find_element_by_id("password").click()
        driver.find_element_by_id("password").clear()
        driver.find_element_by_id("password").send_keys("pass")
        driver.find_element_by_id("password1").click()
        driver.find_element_by_id("password1").clear()
        driver.find_element_by_id("password1").send_keys("123")
        driver.find_element_by_id("password2").click()
        driver.find_element_by_id("password2").clear()
        driver.find_element_by_id("password2").send_keys("123")
        driver.find_element_by_id("btn-change-password").click()
        driver.find_element_by_id("dropdownMenuButton").click()
        driver.find_element_by_id("logOut").click()
        driver.find_element_by_xpath("//button[2]").click()
        driver.find_element_by_id("login-username").click()
        driver.find_element_by_id("login-username").clear()
        driver.find_element_by_id("login-username").send_keys("user")
        driver.find_element_by_id("login-password").click()
        driver.find_element_by_id("login-password").clear()
        driver.find_element_by_id("login-password").send_keys("123")
        driver.find_element_by_id("btn-login").click()
        driver.find_element_by_id("search").click()
        driver.find_element_by_id("search").clear()
        driver.find_element_by_id("search").send_keys("@user1")
        driver.find_element_by_id("search").send_keys(Keys.ENTER)
        driver.find_element_by_id("dropdownMenuButton").click()
        driver.find_element_by_link_text("Profile").click()
        driver.find_element_by_id("delete-profile").click()
        driver.find_element_by_id("delete-profile").click()
        driver.find_element_by_id("delete-profile").click()
        driver.find_element_by_xpath("//button[2]").click()
        driver.find_element_by_id("login-username").click()
        driver.find_element_by_id("login-username").clear()
        driver.find_element_by_id("login-username").send_keys("user")
        driver.find_element_by_id("login-password").clear()
        driver.find_element_by_id("login-password").send_keys("123")
        driver.find_element_by_id("btn-login").click()
        driver.find_element_by_link_text("Register instead").click()
        driver.find_element_by_id("btn-register").click()
        driver.find_element_by_xpath("//img[@alt='logo']").click()
        driver.find_element_by_link_text("Register instead").click()
        driver.find_element_by_id("register-username").click()
        driver.find_element_by_id("register-username").clear()
        driver.find_element_by_id("register-username").send_keys("uros")
        driver.find_element_by_id("register-email").click()
        driver.find_element_by_id("register-email").clear()
        driver.find_element_by_id("register-email").send_keys("u@gmail.com")
        driver.find_element_by_id("register-password").click()
        driver.find_element_by_id("register-password").clear()
        driver.find_element_by_id("register-password").send_keys("123")
        driver.find_element_by_id("register-password-second").click()
        driver.find_element_by_id("register-password-second").clear()
        driver.find_element_by_id("register-password-second").send_keys("123")
        driver.find_element_by_id("btn-register").click()
        # ERROR: Caught exception [ERROR: Unsupported command [selectFrame | index=0 | ]]
        driver.find_element_by_xpath("//span[@id='recaptcha-anchor']/div[5]").click()
        # ERROR: Caught exception [ERROR: Unsupported command [selectFrame | relative=parent | ]]
        driver.find_element_by_id("btn-register").click()
    
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException as e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
