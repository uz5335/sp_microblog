# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re

class HerokuAPP(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = "https://www.katalon.com/"
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_heroku_a_p_p(self):
        driver = self.driver
        driver.find_element_by_id("login-username").click()
        driver.find_element_by_id("login-username").clear()
        driver.find_element_by_id("login-username").send_keys("user")
        driver.find_element_by_id("login-password").click()
        driver.find_element_by_id("login-password").clear()
        driver.find_element_by_id("login-password").send_keys("pass")
        driver.find_element_by_id("btn-login").click()
        driver.find_element_by_id("filter").click()
        driver.find_element_by_css_selector("html.ng-scope").click()
        driver.find_element_by_id("textarea").click()
        driver.find_element_by_id("textarea").clear()
        driver.find_element_by_id("textarea").send_keys("nov tweet...#lol")
        driver.find_element_by_id("btn-new-tweet").click()
        driver.find_element_by_id("search").click()
        driver.find_element_by_id("search").clear()
        driver.find_element_by_id("search").send_keys("#lol")
        driver.find_element_by_id("search").send_keys(Keys.ENTER)
        driver.find_element_by_css_selector("img[alt=\"logo\"]").click()
        # ERROR: Caught exception [ERROR: Unsupported command [mouseOver | css=img[alt="logo"] | ]]
        driver.find_element_by_id("search").click()
        driver.find_element_by_id("search").clear()
        driver.find_element_by_id("search").send_keys("#lol")
        driver.find_element_by_id("search").send_keys(Keys.ENTER)
        driver.find_element_by_css_selector("img[alt=\"logo\"]").click()
        # ERROR: Caught exception [ERROR: Unsupported command [mouseOver | css=img[alt="logo"] | ]]
        driver.find_element_by_id("textarea").click()
        driver.find_element_by_id("textarea").clear()
        driver.find_element_by_id("textarea").send_keys("#lol")
        driver.find_element_by_id("btn-new-tweet").click()
        driver.find_element_by_id("search").click()
        driver.find_element_by_id("search").clear()
        driver.find_element_by_id("search").send_keys("#lol")
        driver.find_element_by_id("search").send_keys(Keys.ENTER)
        driver.find_element_by_css_selector("a.glyphicon.glyphicon-trash.ng-scope").click()
        driver.find_element_by_css_selector("button.tingle-btn.tingle-btn--pull-right.tingle-btn--danger").click()
        driver.find_element_by_css_selector("img[alt=\"logo\"]").click()
        # ERROR: Caught exception [ERROR: Unsupported command [mouseOver | css=img[alt="logo"] | ]]
        driver.find_element_by_xpath("//tweetcard[2]/div/div[2]/a[2]").click()
        driver.find_element_by_css_selector("button.tingle-btn.tingle-btn--pull-right.tingle-btn--danger").click()
        driver.find_element_by_id("dropdownMenuButton").click()
        driver.find_element_by_link_text("Following").click()
        driver.find_element_by_css_selector("div.col-xs-9.text-left > h3.activeUser-name.ng-binding").click()
        driver.find_element_by_css_selector("img[alt=\"logo\"]").click()
        # ERROR: Caught exception [ERROR: Unsupported command [mouseOver | css=img[alt="logo"] | ]]
        driver.find_element_by_id("dropdown-style").click()
        driver.find_element_by_id("dropdown-style").click()
        driver.find_element_by_id("dropdownMenuButton").click()
        driver.find_element_by_link_text("Profile").click()
        driver.find_element_by_id("textarea1").click()
        driver.find_element_by_id("textarea1").clear()
        driver.find_element_by_id("textarea1").send_keys("nov opis")
        driver.find_element_by_id("btn-change-description").click()
        driver.find_element_by_id("password").click()
        driver.find_element_by_id("password").clear()
        driver.find_element_by_id("password").send_keys("1")
        driver.find_element_by_id("password1").click()
        driver.find_element_by_id("password1").clear()
        driver.find_element_by_id("password1").send_keys("d")
        driver.find_element_by_id("password2").click()
        driver.find_element_by_id("password2").clear()
        driver.find_element_by_id("password2").send_keys("d")
        driver.find_element_by_id("btn-change-password").click()
        driver.find_element_by_id("password").clear()
        driver.find_element_by_id("password").send_keys("")
        driver.find_element_by_id("password1").clear()
        driver.find_element_by_id("password1").send_keys("")
        driver.find_element_by_id("password2").clear()
        driver.find_element_by_id("password2").send_keys("")
        driver.find_element_by_id("password").click()
        driver.find_element_by_id("password").clear()
        driver.find_element_by_id("password").send_keys("pass")
        driver.find_element_by_id("password1").clear()
        driver.find_element_by_id("password1").send_keys("123")
        driver.find_element_by_id("password2").click()
        driver.find_element_by_id("password2").clear()
        driver.find_element_by_id("password2").send_keys("123")
        driver.find_element_by_xpath("//div[4]").click()
        driver.find_element_by_id("btn-change-password").click()
        driver.find_element_by_id("dropdownMenuButton").click()
        driver.find_element_by_id("logOut").click()
        driver.find_element_by_css_selector("button.tingle-btn.tingle-btn--pull-right.tingle-btn--danger").click()
        driver.find_element_by_id("login-username").click()
        driver.find_element_by_id("login-username").clear()
        driver.find_element_by_id("login-username").send_keys("user")
        driver.find_element_by_id("login-password").click()
        driver.find_element_by_id("login-password").clear()
        driver.find_element_by_id("login-password").send_keys("pass")
        driver.find_element_by_id("btn-login").click()
        driver.find_element_by_id("login-username").click()
        driver.find_element_by_id("login-username").click()
        driver.find_element_by_id("login-username").clear()
        driver.find_element_by_id("login-username").send_keys("")
        driver.find_element_by_id("login-password").click()
        driver.find_element_by_id("login-password").click()
        driver.find_element_by_id("login-password").clear()
        driver.find_element_by_id("login-password").send_keys("")
        driver.find_element_by_id("login-username").click()
        driver.find_element_by_id("login-username").click()
        driver.find_element_by_id("login-username").clear()
        driver.find_element_by_id("login-username").send_keys("user")
        driver.find_element_by_id("login-password").click()
        driver.find_element_by_id("login-password").clear()
        driver.find_element_by_id("login-password").send_keys("pass")
        driver.find_element_by_id("btn-login").click()
        driver.find_element_by_id("login-password").click()
        driver.find_element_by_id("login-password").clear()
        driver.find_element_by_id("login-password").send_keys("")
        driver.find_element_by_id("login-username").click()
        driver.find_element_by_id("login-username").clear()
        driver.find_element_by_id("login-username").send_keys("")
        driver.find_element_by_id("login-password").click()
        driver.find_element_by_id("login-username").click()
        driver.find_element_by_id("login-username").clear()
        driver.find_element_by_id("login-username").send_keys("user")
        driver.find_element_by_id("login-password").click()
        driver.find_element_by_id("login-password").click()
        driver.find_element_by_id("login-password").clear()
        driver.find_element_by_id("login-password").send_keys("123")
        driver.find_element_by_id("btn-login").click()
        driver.find_element_by_id("dropdownMenuButton").click()
        driver.find_element_by_link_text("Profile").click()
        # ERROR: Caught exception [ERROR: Unsupported command [mouseOver | link=Profile | ]]
        driver.find_element_by_id("delete-profile").click()
        driver.find_element_by_css_selector("button.tingle-btn.tingle-btn--pull-right.tingle-btn--danger").click()
        # ERROR: Caught exception [ERROR: Unsupported command [mouseOver | link=Register instead | ]]
    
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException as e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
