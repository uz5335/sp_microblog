function unLike(username, messageId) {
    console.log("clicked unlike")
    unlikeRequest(username, messageId);
}

function addLike(username, messageId) {
    console.log("clicked add like")
    likeRequest(username, messageId);
}
//
// //todo zrihtaj da bo klican "unlike"  in ga napisi
//
function addRetweet(userId, messageId) {
    console.log("clicked add retweet")
    retweetRequest(userId, messageId);
}

//$("#user-feed").html("");
var all_userFeed = function (user_id) {
    getUserFeed(user_id, function (err, tweets) {
        if (err) {
            console.log(err);
        } else {
            console.log(tweets);
            $("#user-feed").html("");
            for (let i = 0; i < tweets.length; i++) {
                prependTweet(tweets[i], "#user-feed");
            }
        }
    });
};


function removeTweet(event, tweetId) {
    console.log(tweetId);

    event.preventDefault();

    var forRemove = $(event.currentTarget)
        .parent()
        .parent()
        .parent()

    //console.log(forRemove);
    var modal = getModal()

    modal.setContent('<h1>Are you sure you want to delete this tweet?</h1>');

// add a button
    modal.addFooterBtn('Cancel', 'tingle-btn tingle-btn--primary', function () {
        modal.close();
    });

// add another button
    modal.addFooterBtn('Delete', 'tingle-btn tingle-btn--pull-right tingle-btn--danger', function () {
        //poklici iz API-JA da odstrnis tweet...
        removeUserTweet(tweetId, function (err, ok) {
            if (err) {
                console.log(err)
            } else {
                forRemove.remove();
                modal.close();
            }
        });

    });
    modal.open()
}


function prependTweet(tweet, where) {
    ////////////tukaj pri dodajanju novega tweeta je potrebno se message id urediti./////////////////////////////////////////////////
    let isMine = tweet.creator._id === activeUser;
    let canRetweet = false;
    for (let j = 0; j < tweet.retweets.length; j++) {
        if (tweet.retweets[j].userId === activeUser) {
            console.log(tweet.body, " ", tweet.retweets[j].userId, " ", activeUser);
            canRetweet = true;
            break;
        }
    }
    let isLiked = tweet.likes.includes(activeUser);
    
    let addLikeStr;
    if(isMine){
        addLikeStr = "";
    }else if(isLiked){
        addLikeStr = `onclick=unLike('${activeUser}','${tweet._id}')`;
    }else{
        addLikeStr = `onclick=addLike('${activeUser}','${tweet._id}')`
    }
    
            
    let   trashStr = isMine
            ? `<a class="glyphicon glyphicon-trash" onclick="removeTweet(event, '${tweet._id}')"></a>`
            : "";
        // addRetweetStr = `<a class="glyphicon glyphicon-trash" onclick="addRetweet('${activeUser}','${tweet._id}')"></a>`
    let  addRetweetStr = (isMine || canRetweet)? "" :`<a onclick="addRetweet('${activeUser}', '${tweet._id}')"></a>`;

    //sprehodi se po vseh retweetih in poglej ce je aktivni username notri....
  

    let searcUser = `onclick=searchClickedUser("${tweet.creator.username}")`;
    //console.log(searcUser);

    //ce je a element cez vec vrstic se sam zapre, tko da je pol closing tag prikazan kot text.
    $(`${where}`).prepend(` 
            <div class="card" id = "${tweet._id}">\ 
                <div class="card-section tweetcard-header">\ 
                    <div ${searcUser} class= "tweetcard-userinfo"><img src="/images/user-icon.png" class="img-rounded"><b>${
        tweet.creator.username
        }</b></div>    \ 
                        <div class="tweetcard-controls">\ 
                            
            <a class="glyphicon glyphicon-retweet ${
        canRetweet ||
        isMine
            ? "tweetcard-controls-unavailable"
            : ""
        }" ${addRetweetStr}</a>
                              <span>${tweet.retweets.length}</span>
                            <a class="glyphicon glyphicon-heart ${
        tweet.likes.includes(activeUser) 
            ? "tweetcard-controls-unlike"
            : ""
        } ${isMine?"tweetcard-controls-unavailable":""}" ${addLikeStr}></a>\
                            <span>${tweet.likes.length}</span>
                            ${trashStr}
                        </div>
                    </div>\ 
                <div class="card-section"><p class="text-card">${tweet.body}</p></div>\ 
            </div>
        `);
}





