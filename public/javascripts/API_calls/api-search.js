function searchSomething(username,hashtag,callback){
          $.ajax({
            type: "GET",
            url: "api/search?usernames="+username+"&hashtags="+hashtag,
            // The key needs to match your method's input parameter (case-sensitive).
            success: function(data){
                        //console.log(data);
        				callback(null, data);
        			},
            statusCode: {
                500: function(err) {
                  getAlertModal(err);
                },
                404: function(err){
                    callback(err);
                  
                }
              }
            
        });
}