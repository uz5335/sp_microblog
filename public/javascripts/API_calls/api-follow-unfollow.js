function getAllRecommendation(user_id, callback){
    $.ajax({
        type: "GET",
        url: "api/user/recommend/"+user_id,
        // The key needs to match your method's input parameter (case-sensitive).
        success: function(data){
                    //console.log(data);
    				callback(null, data);
    			},
        statusCode: {
            500: function(err) {
              alert(err);
            },
            404: function(err){
                callback(err);
            }
          }
        
    });
}

function getAllFollowing(user_id, callback){
    $.ajax({
        type: "GET",
        url: "api/user/following/"+user_id,
        // The key needs to match your method's input parameter (case-sensitive).
        success: function(data){
                    //console.log(data);
    				callback(null, data);
    			},
        statusCode: {
            500: function(err) {
              alert(err);
            },
            404: function(err){
                callback(err);
              
            }
          }
        
    });
}

function userFollow(user_id, follower_id, callback){
     let data1 = {
            userId: user_id,
            followerId: follower_id
        }
      $.ajax({
        type: "POST",
        url: "api/user/follow",
        // The key needs to match your method's input parameter (case-sensitive).
        data: JSON.stringify(data1),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(data){
                    //console.log(data);
    				callback(null, data);
    			    
    			},
        statusCode: {
            500: function(err) {
              alert(err);
            },
            404: function(err){
                callback(err);
              
            }
          }
    });
}

function userUNFollow(user_id, follower_id, callback){
     let data1 = {
            userId: user_id,
            followerId: follower_id
        }
      $.ajax({
        type: "POST",
        url: "api/user/unfollow",
        // The key needs to match your method's input parameter (case-sensitive).
        data: JSON.stringify(data1),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(data){
                    //console.log(data);
    				callback(null, data);
    			    
    			},
        statusCode: {
            500: function(err) {
              alert(err);
            },
            404: function(err){
                callback(err);
              
            }
          }
        
    });
}

