function retweetRequest(userId, tweetId) {
    console.log(userId, tweetId, "clicked");
    let tweetData = {
        userId: userId
    };
    $.ajax({
        type: "POST",
        url: "api/tweet/" + tweetId + "/retweet",
        headers: tweetData,
        success: function (data) {
            //console.log(data)
            all_userFeed(userId);
        },
        statusCode: {
            500: function (err) {
                getAlertModal("There was an error: " + err);
                // alert(err);
            },
            404: function (err) {
                getAlertModal("There was an error: " + err);
                callback(err);
            }
        }
    });
}

function likeRequest(userId, tweetId) {
    console.log(userId, tweetId);
    let tweetData = {
        userId: userId
    };
    $.ajax({
        type: "POST",
        url: "api/tweet/" + tweetId + "/like",
        headers: tweetData,
        success: function (data) {
            console.log(data)
            all_userFeed(userId);
        },
        statusCode: {
            500: function (err) {
                getAlertModal("There was an error: " + err);
                // alert(err);
            },
            404: function (err) {
                getAlertModal("There was an error: " + err);
                //callback(err);

            }
        }
    });
}

function unlikeRequest(userId, tweetId) {
    let tweetData = {
        userId: userId
    };
    $.ajax({
        type: "POST",
        url: "api/tweet/" + tweetId + "/unlike",
        headers: tweetData,
        success: function (data) {
            console.log(data)
            all_userFeed(userId);
        },
        statusCode: {
            500: function (err) {
                getAlertModal("There was an error: " + err);
                // alert(err);
            },
            404: function (err) {
                getAlertModal("There was an error: " + err);
                //callback(err);

            }
        }
    });
}