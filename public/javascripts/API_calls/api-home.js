

function newUserTweet(user_id, message, callback){
     //console.log("notri delam klic...")
        let tweetData = {
            userId: user_id,
            body: message
        }
          $.ajax({
            type: "PUT",
            url: "api/tweet",
            // The key needs to match your method's input parameter (case-sensitive).
            data: JSON.stringify(tweetData),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(data){
                        //console.log(data);
        				callback(null, data);
        			    
        			},
            statusCode: {
                500: function(err) {
                  alert(err);
                },
                404: function(err){
                    callback(err);
                  
                }
              }
            
        });
}

function getUserFeed(user_id, callback){
    $.ajax({
        type: "GET",
        url: "api/feed/"+user_id,
        // The key needs to match your method's input parameter (case-sensitive).
        success: function(data){
                    //console.log(data);
    				callback(null, data);
    			},
        statusCode: {
            500: function(err) {
              alert(err);
            },
            404: function(err){
                callback(err);
              
            }
          }
        
    });
}

function removeUserTweet(tweet_id, callback){
    $.ajax({
        type: "DELETE",
        url: "api/tweet/"+tweet_id,
        // The key needs to match your method's input parameter (case-sensitive).
        success: function(data){
                    //console.log(data);
    				callback(null, data);
    			},
        statusCode: {
            500: function(err) {
              alert(err);
            },
            404: function(err){
                callback(err);
              
            }
          }
        
    });
}

function getOneTweet(tweet_id, callback){
    $.ajax({
        type: "GET",
        url: "api/tweet/"+tweet_id,
        // The key needs to match your method's input parameter (case-sensitive).
        success: function(data){
                    //console.log(data);
    				callback(null, data);
    			},
        statusCode: {
            500: function(err) {
              alert(err);
            },
            404: function(err){
                callback(err);
              
            }
          }
        
    });
}