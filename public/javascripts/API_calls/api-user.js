function getUser(user_id, callback){
          $.ajax({
            type: "GET",
            url: "api/user/"+user_id,
            // The key needs to match your method's input parameter (case-sensitive).
            success: function(data){
                        //console.log(data);
        				callback(null, data);
        			},
            statusCode: {
                500: function(err) {
                  alert(err);
                },
                404: function(err){
                    callback(err);
                  
                }
              }
            
        });
}

function registerUser(username,password,callback){
     let data1 = {
            username: username,
            password: password
        }
      $.ajax({
        type: "PUT",
        url: "api/user",
        // The key needs to match your method's input parameter (case-sensitive).
        data: JSON.stringify(data1),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(data){
                    //console.log(data);
    				callback(null, data);
    			},
        statusCode: {
            500: function(err) {
              alert(err);
            },
            404: function(err){
                callback(err);
              
            }
          }
    });
}

function getUserByUsername(username, callback){
          $.ajax({
            type: "GET",
            url: "api/user/username/"+username,
            // The key needs to match your method's input parameter (case-sensitive).
            success: function(data){
                        //console.log(data);
        				callback(null, data);
        			},
            statusCode: {
                500: function(err) {
                  alert(err);
                },
                404: function(err){
                    callback(err);
                  
                }
              }
            
        });
}

function userDescriptionChange(user_id, new_description, callback){
      let data1 = {
            userId: user_id,
            newDescription: new_description
        }
      $.ajax({
        type: "POST",
        url: "api/user/changeDescription",
        // The key needs to match your method's input parameter (case-sensitive).
        data: JSON.stringify(data1),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(data){
                    //console.log(data);
    				callback(null, data);
    			    
    			},
        statusCode: {
            500: function(err) {
              alert(err);
            },
            404: function(err){
                callback(err);
              
            }
          }
    });
}

function userPasswordChange(user_id, new_password, old_password,callback){
       let data1 = {
            userId: user_id,
            newPassword: new_password,
            oldPassword: old_password
        }
      $.ajax({
        type: "POST",
        url: "api/user/changePassword",
        // The key needs to match your method's input parameter (case-sensitive).
        data: JSON.stringify(data1),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(data){
                    //console.log(data);
    				callback(null, data);
    			    
    			},
        statusCode: {
            500: function(err) {
              alert(err);
            },
            404: function(err){
                callback(err);
              
            }
          }
    });
}

function deleteUserProfile (user_id, callback){
         $.ajax({
            type: "DELETE",
            url: "api/user/"+user_id,
            // The key needs to match your method's input parameter (case-sensitive).
            success: function(data){
                        //console.log(data);
        				callback(null, data);
        			},
            statusCode: {
                500: function(err) {
                  alert(err);
                },
                404: function(err){
                    callback(err);
                  
                }
              }
            
        });
}