//google prijava...
function onSignIn(googleUser) {
    var profile = googleUser.getBasicProfile();
    console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
    console.log('Name: ' + profile.getName());
    console.log('Image URL: ' + profile.getImageUrl());
    console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.

    var data = JSON.parse(sessionStorage.getItem("data"));
    //preveri, ce je ime ze v bazi podatkov....
    var isRegistered = false;
    for (var i = 0; i < data.length; i++) {
        if (data[i].username === profile.getName()) { //na ta usename je ze nekaj resistrirano.
            isRegistered = true;
            sessionStorage.setItem("data", JSON.stringify(data));
            sessionStorage.setItem("activeUser", JSON.stringify(data[i]));
            break;
        }
    }
    if (isRegistered === true) {
        window.location.href = "/home";
    } else {
        var split = profile.getName().split(" ");
        console.log(split);
        var u = split.join();
        console.log(u);
        var newUser = {
            username: u,
            password: profile.getId(),
            description: "",
            following: [],
            tweets: []
        }
        var data = JSON.parse(sessionStorage.getItem("data"));
        data.push(newUser);
        console.log(data);
        sessionStorage.setItem("data", JSON.stringify(data));
        sessionStorage.setItem("activeUser", JSON.stringify(newUser));
        window.location.href = "/home";
    }

}