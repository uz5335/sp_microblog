"use strict";
//skripta je tako ali tako ali tako na isti strani kot je searchController in ni potrebe po dodatnem preverjanju, ker se ze enkrat preveri.
//vendar za enkrat sem pusitil stvari notri.
if (!sessionStorage.getItem("activeUser")) {
  //window.location.replace("/login");
  window.location.href = "/login";
  console.log("preusmerjam");
}
var activeUser = sessionStorage.getItem("activeUser"); //iz tega session controllerja dobimo trenutno prijavljenega uporabnika in vse njegove podatke in tweete.
console.log(activeUser);

var what_searched = sessionStorage.getItem("search");

function showSearchUserInfo_Unfollowej(
  user_name,
  numFollowing,
  numFollowers,
  description,
  user_id
) {
  $("#search-user-info").html("");
  let searcUser = `onclick=searchClickedUser('${user_name}')`;
  $("#search-user-info").append(`
                                       <div class="card">\ 
                                       <div class="card-section">\ 
                                            <div ${searcUser} class="row onHoverUserSearch">\ 
                                                <div class="col-xs-3"><span class="glyphicon glyphicon-user vecjiGlyph"></span></div>\ 
                                                <div class="col-xs-9 text-left"><h3>${
                                                  user_name
                                                }</h3></div>\ 
                                                </div>\ 
                                            <div class="row">\ 
                                                <div class="col-sm-6 col-xs-3"><span class="drugaBarva">Following: ${
                                                  numFollowing
                                                }</span></div>\ 
                                                <div class="col-sm-6 col-xs-3"><span class="drugaBarva">Followers: ${
                                                  numFollowers
                                                }</span></div>\ 
                                                
                                                </div>\ 
                                       </div>\ 
                                       <div class="card-section"><span>${
                                         description
                                       }</span></div>\ 
                                       <div class="card-section" style="text-align: center"><btn class="btn"style="width: 95%" onclick="unFollwej(event, '${user_id}')">Unfollow</btn></div>\ 
                                       </div>\ 
                                        `);
}

function showSearchUserInfo_Followej(
  user_name,
  numFollowing,
  numFollowers,
  description,
  user_id
) {
  $("#search-user-info").html("");
  let searcUser = `onclick=searchClickedUser('${user_name}')`;
  $("#search-user-info").append(`
                                       <div class="card">\ 
                                       <div class="card-section">\ 
                                            <div ${searcUser} class="row onHoverUserSearch">\ 
                                                <div class="col-xs-3"><span class="glyphicon glyphicon-user vecjiGlyph"></span></div>\ 
                                                <div class="col-xs-9 text-left"><h3>${
                                                  user_name
                                                }</h3></div>\ 
                                                </div>\ 
                                            <div class="row">\ 
                                                <div class="col-sm-6 col-xs-3"><span class="drugaBarva">Following: ${
                                                  numFollowing
                                                }</span></div>\ 
                                                <div class="col-sm-6 col-xs-3"><span class="drugaBarva">Followers: ${
                                                  numFollowers
                                                }</span></div>\ 
                                                
                                                </div>\ 
                                       </div>\ 
                                       <div class="card-section"><span>${
                                         description
                                       }</span></div>\ 
                                       <div class="card-section" style="text-align: center"><btn class="btn"style="width: 95%" onclick="followej(event, '${user_id}')">Follow</btn></div>\ 
                                       </div>\ 
                                        `);
}

function unFollwej(event, user_id) {
  console.log(user_id);
  userUNFollow(user_id, activeUser, function(err, ok){
    if(err){
        console.log(err);
    }else{
        getUser(user_id, function(err, user){
            if(err){
                console.log(err);
            }else{
                showSearchUserInfo_Followej(
                user.username,
                user.following.length,
                user.followers.length,
                user.description.body,
                user._id
              );
            }
        })  
    }
  })
}

function followej(event, user_id) {
  console.log("drugic");
    userFollow(user_id, activeUser, function(err, ok){
      if(err){
          console.log(err);
      }else{
          getUser(user_id, function(err, user){
          if(err){
              console.log(err);
          }else{
              console.log(user);
                showSearchUserInfo_Unfollowej(
                user.username,
                user.following.length,
                user.followers.length,
                user.description.body,
                user._id
              );
          }
  })  
         
      }
  })

}



$(document).ready(function() {
  
     
  getUser(activeUser, function(err, user){
      if(err){
          console.log(err);
      }else{
          //console.log(user);
        $(".activeUser-name").text(user.username);
      }
  })  
  
  //Event lisnter za enter pri search barru.
  $("#search").val(what_searched);
  $(".activeUser-name").text(activeUser.username);
  //prikaz bo odvisen od tega kaj se je iskalo.
  if (what_searched.charAt(0) === "@") {
    //prikazi uporabnika
    //console.log("what!!!!!!!!!!!!!", what_searched);
    $("#search-feed").append(`<h3>Nothing was found here!!!</h3>`);
    var search_user = what_searched.substr(1);
    console.log(search_user);
    searchSomething(search_user, "", function(err, tweets){
      if(err){
        console.log(err);
      }else{
        console.log(tweets);
        $("#search-feed").html("");
        for(let j = 0; j < tweets.length; j++){
          prependTweet(tweets[j], "#search-feed");
        }
      }
    });
    
    getAllRecommendation(activeUser, function(err, users){
      if(err){
        console.log(err);
      }else{
        console.log(users);
        let najden = false;
        for(let j = 0; j < users.length; j++){
          console.log(users[j].username + "  " + search_user);
          if(users[j].username === search_user){
            console.log("neki neki")
            showSearchUserInfo_Followej(
              users[j].username,
              users[j].following.length,
              users[j].followers.length,
              users[j].description.body,
              users[j]._id
            );
            najden = true;
            break;
          }
        }
        if(najden == false){
           getUserByUsername(search_user, function(err, user){
              if(err){
                  console.log(err);
              }else{
                showSearchUserInfo_Unfollowej(
                  user.username,
                  user.following.length,
                  user.followers.length,
                  user.description.body,
                  user._id
                );
              }
          });
        }
      }
    });
    
  } else {
    //prikazi #
    //pojdi cez vsa sporocila. poisi #string in prikazi to sporocilo, ce je ta #notri vsebovan.
    let hashtags = what_searched.substr(1);
     $("#search-feed").append(`<h3>Nothing was found here!!!</h3>`);
    console.log(hashtags);
    searchSomething("", hashtags, function(err, tweets){
      if(err){
        console.log(err);
      }
      else{
        if(tweets.length > 0){
           $("#search-feed").html("");  
        }
        for(let j = 0; j < tweets.length; j++){
          prependTweet(tweets[j], "#search-feed");
        }
      }
    });
  }
});
