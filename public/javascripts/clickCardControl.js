"use strict";
function searchClickedUser(username) {
    getUserByUsername(username, function(err, user){
        if(err){
            console.log(err);
        }else{
            if(user._id === activeUser){
                window.location.href = "/home";        
            }else{
                sessionStorage.setItem("search", '@' + username); //nastavim, kaj je uporabnik iskal, da bom lahko potem v search oknu nasel in ustrezno prikazal
                window.location.href = "/search"; //preusmeritev na search.             
            }
        }
    });

}