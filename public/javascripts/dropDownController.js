"use strict";
$(document).ready(function () {
    $("#logOut").click(function (event) {
        event.preventDefault();

        var modal = getModal();
        modal.setContent('<h1>Are you sure you want to log out?</h1>');

        modal.addFooterBtn('Cancel', 'tingle-btn tingle-btn--primary', function () {
            modal.close();
        });

        modal.addFooterBtn('Yes', 'tingle-btn tingle-btn--pull-right tingle-btn--danger', function () {
            sessionStorage.removeItem("activeUser");
            //todo enable back when you complete google registration
            //signOut();
            window.location.href = "/login";
            modal.close();
        });
        modal.open()
    });
});


// function signOut() {
//     var auth2 = gapi.auth2.getAuthInstance();
//     auth2.signOut().then(function () {
//         console.log('User signed out.');
//     });
// }