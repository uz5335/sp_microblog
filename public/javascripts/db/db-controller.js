"use strict";
$(document).ready(function () {
    console.log(sessionStorage.getItem("activeUser"));
    $("#btn-deleteCollection").click(function (event) {
        event.preventDefault();
        //console.log("buttonClicked");
        //sessionStorage.removeItem("activeUser");
        $.ajax({
            type: "DELETE",
            url: "api/db/delete",
            // The key needs to match your method's input parameter (case-sensitive).
            //data: JSON.stringify(markers),
            //contentType: "application/json; charset=utf-8",
            //dataType: "json",
            success: function(data){
        				//alert(data);
        			    alert("DB deleted")
        			   // $("#btn-initDB").prop("disabled",false);
        			},
            failure: function(errMsg) {
                alert(errMsg);
            }
        });
    });
    
    $("#btn-initDB").click(function (event) {
        event.preventDefault();
        //console.log("buttonClicked");
         //sessionStorage.removeItem("activeUser");
        $.ajax({
            type: "PUT",
            url: "api/db/init",
            success: function(data){
        				//alert(data);
        			    alert("succefuly created database")
        			    //$("#btn-initDB").prop("disabled",true);
        			},
            failure: function(errMsg) {
                alert("database already exist");
            },
           statusCode: {
                500: function() {
                  alert("database already exist");
                }
              }
        });
      
    });
});

