"use strict";

$(document).ready(function () {
    var validateEmail = function (value) {
        var regex = new RegExp("^[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,3}$");
        return regex.test(value);
    };

    $("#btn-register").click(function (event) {
        event.preventDefault();
        var u = $("#register-username").val();
        var pass1 = $("#register-password").val();
        var pass2 = $("#register-password-second").val();
        var email = $("#register-email").val();

        if (u === "" || pass1 === "" || pass2 === "" || email === "") {
            getAlertModal('Input field cannot be empty. Type something');
        } else if (!validateEmail(email)) {
            getAlertModal("Wrong e-mail format");
            $("#register-email").val("");

            //todo sem spravi API klic
        } else {
            var checkCorrect = u.split(" ");

            //to kar pusti
            if (checkCorrect.length > 1) {
                getAlertModal("Username cannot contain spaces");
            }
            else if (u.length >= 13) {
                getAlertModal("Username can only be up to 13 charachers long");
            }
            //preveri ali se vneseni gesli ujemata
            else if (pass1 === pass2) {
                //tukaj naredi API klic
                registerUser(u, pass1, function(err, user){
                    if(err){
                        console.log(err);
                    }else{
                        sessionStorage.setItem("activeUser", user._id);
                        window.location.href = "/home";
                    }
                })
            }
            else {
                getAlertModal("Passwords do not match");
                $("#register-username").val("");
                $("#register-password").val("");
            }
        }
    });


});

  