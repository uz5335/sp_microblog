//ko prides na home page preveri ali je kateri uporabnik prijavljen.
"use strict";
if (!sessionStorage.getItem("activeUser")) {
    //window.location.replace("/login");
    window.location.href = "/login";
    console.log("preusmerjam");
}
//ce je kateri prijavljen se koda izvaja naprej. drugace je pa zograj napisan redirect na login screen.
var activeUser = sessionStorage.getItem("activeUser"); //iz tega session controllerja dobimo trenutno prijavljenega uporabnika in vse njegove podatke in tweete.
console.log(activeUser);


$(document).ready(function () {
    //Event lisnter za enter pri search barru.
    $("#search").keypress(function (event) {
        var key = event.which;
        if (key === 13) {
            //alert("To be conitnued");
            var what_searched = $("#search")
                .val()
                .trim();
            if (what_searched === "") {
                // user did not search for anything.
                $("#search").val();
            } else {
                var firstChar = what_searched.charAt(0);
                if (firstChar === "@" || firstChar === "#") {
                    console.log("pravilen vnos!!!!");
                    if (what_searched.length > 1) {
                        console.log("pravilen vnos!!!!");
                        var search_user = what_searched.substr(1); //preveri, ce je iskani uporabnik kar ta uporabnik in takoj preumeri.
                        /*if (search_user === activeUser.username) {
                            window.location.href = "/home"; //ce uporabnik isce samega sebe, mu prikazi kar glavni zaslon
                        } */
                        getUserByUsername(search_user, function(err, user){
                            if(err){
                                //console.log(err);
                                sessionStorage.setItem("search", what_searched); //nastavim, kaj je uporabnik iskal, da bom lahko potem v search oknu nasel in ustrezno prikazal
                                window.location.href = "/search"; //preusmeritev na search.
                            }else{
                                if(user._id == activeUser){
                                     window.location.href = "/home";
                                }
                                else{
                                    sessionStorage.setItem("search", what_searched); //nastavim, kaj je uporabnik iskal, da bom lahko potem v search oknu nasel in ustrezno prikazal
                                    window.location.href = "/search"; //preusmeritev na search.
                                }
                            }
                        });
                    
                    } else {
                        getAlertModal("Incomplete search request");
                        $("#search").val("");
                    }
                } else {
                    getAlertModal("If you are searching by users use @ otherwise use #");
                    $("#search").val("");
                }
            }
        }
    });
});
