"use strict";
$(document).ready(function () {
    $("#btn-login").click(function (event) {
        event.preventDefault();
        //console.log("buttonClicked");
        var username1 = $("#login-username").val();
        var password1 = $("#login-password").val();
        console.log(" ", username1, " ", password1);
        if (username1 == "" || password1 == "") {
            getAlertModal("Some fields are empty");
        } else {
            //preveri ce se kaksen username in password ujema z vnosom v data modelu sessionStorage.getItem("data")
            loginCheck(username1, password1, function (err, user) {
                if (err) {
                    getAlertModal("Wrong username or password");
                    $("#login-username").val("");
                    $("#login-password").val("");
                }
                else {
                    console.log(user);
                    sessionStorage.setItem("activeUser", user._id); //shrani si v activeUser vse info o uporabniku.
                    window.location.href = "/home";
                }
            })

        }
        //console.log(" ", username1, " ", password1);
    });
});

function loginCheck(Username, Password, callback) {
    let userData = {
        username: Username,
        password: Password
    }
    $.ajax({
        type: "POST",
        url: "api/user/login",
        // The key needs to match your method's input parameter (case-sensitive).
        data: JSON.stringify(userData),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, txtStatus, request) {
            console.log("data");
            console.log(data);
            window.loginRespone = data;
            let a = request.getResponseHeader("jwt")
            console.log("afasfasfsafsaf");
            console.log(a);

            callback(null, data);

        },
        statusCode: {
            500: function (err) {
                getAlertModal(err);
            },
            404: function (err) {
                callback(err);

            }
        }

    });
}

