//ko prides na home page preveri ali je kateri uporabnik prijavljen.
"use strict";
if (!sessionStorage.getItem("activeUser")) {
    //window.location.replace("/login");
    window.location.href = "/login";
    console.log("preusmerjam");
}

var activeUser = sessionStorage.getItem("activeUser"); //iz tega session controllerja dobimo trenutno prijavljenega uporabnika in vse njegove podatke in tweete.
console.log(activeUser);

//ce je kateri prijavljen se koda izvaja naprej. drugace je pa zograj napisan redirect na login screen.
$(document).ready(function () {
 getUser(activeUser, function(err, user){
      if(err){
          console.log(err);
      }else{
          //console.log(user);
        $(".activeUser-name").text(user.username);
        $(".activeUser-description").text(user.description.body);
        $(".activeUser-numFollowgin").text(user.following.length);
        $(".activeUser-numFollowers").text(user.followers.length);
      }
  })  
    $("#goGome").click(function () {
        window.location.href = "/home";
    });

    //dodaj eventLister za pritisk na gumb za spremembo teksta in potem aktivnemu uporabniku spremeni nastavitve. tudi v data-modelu naredi spremembo.
    $("#btn-change-description").click(function (event) {
        event.preventDefault();
        //console.log("click!!!!!!");
        var description_change = $("#textarea1").val();
        if(description_change == ""){
            getAlertModal("Input filed can not be empty");
        }
        else if (description_change.length > 250) {
            getAlertModal("Description can only be 250 characters long");
            $("#textarea1").val();
        } else {
           userDescriptionChange(activeUser, description_change, function(err, ok){
               if(err){
                   console.log(err);
               }else{
                   getUser(activeUser, function(err, user){
                      if(err){
                          console.log(err);
                      }else{
                        $(".activeUser-description").text(user.description.body);
                      }
                  })  
               }
           });
           
            $("#textarea1").val("");
        }
    });

    //dodajmo se event listener za moznost sremembe gesla
    $("#btn-change-password").click(function (event) {
        event.preventDefault();
        //console.log("clikc");
        var oldPass = $("#password").val();
        var newPass1 = $("#password1").val();
        var newPass2 = $("#password2").val();
        $("#password").val("");
        $("#password1").val("");
        $("#password2").val("");
        if (oldPass === "" || newPass1 === "" || newPass2 === "") {
            getAlertModal("Input fields are empty");
        } else {
            if(newPass1 !== newPass2){
                getAlertModal("Passwords do not match");
            }else{
                userPasswordChange(activeUser, newPass2, oldPass, function(err, ok){
                    if(err){
                         getAlertModal("new and old pass do not match");
                    }else{
                         getAlertModal("Success! Yout password has been changed");
                    }
                })
            }
        }
    });
    
       //dodajmo se event listener za moznost sremembe gesla
    $("#delete-profile").click(function (event) {
        event.preventDefault();
        console.log("clikc");
         var modal = getModal();
        modal.setContent('<h1>do you really want to delete the profile?</h1>');

        modal.addFooterBtn('Cancel', 'tingle-btn tingle-btn--primary', function () {
            modal.close();
        });

        modal.addFooterBtn('Yes', 'tingle-btn tingle-btn--pull-right tingle-btn--danger', function () {
            deleteUserProfile(activeUser, function(err, ok){
                if(err){
                    getAlertModal(err);
                }else{
                    //getAlertModal("Profile deleted");
                    sessionStorage.removeItem("activeUser");
                    window.location.href = "/login";
                }
            })
            modal.close();
        });
        modal.open()
      
    });
});
