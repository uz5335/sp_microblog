//ko prides na home page preveri ali je kateri uporabnik prijavljen.
"use strict";
if (!sessionStorage.getItem("activeUser")) {
    //window.location.replace("/login");
    window.location.href = "/login";
    console.log("preusmerjam");
}
//ce je kateri prijavljen se koda izvaja naprej. drugace je pa zograj napisan redirect na login screen.
var activeUser = sessionStorage.getItem("activeUser"); //iz tega session controllerja dobimo trenutno prijavljenega uporabnika in vse njegove podatke in tweete.
console.log(activeUser);

function unFollwej(event, user_id) {
    userUNFollow(user_id, activeUser, function(err, ok){
        if(err){
            console.log(err);
        }else{
            getUser(activeUser, function(err, user){
                if(err){
                    console.log(err);
                }else{
                    $(".activeUser-numFollowgin").text(user.following.length);
                }
            })  
            
        }
    })
    
    $(event.currentTarget)
        .parent()
        .parent()
        .parent()
        .remove();
}

function displayFollowing(user){
    let searcUser = `onclick=searchClickedUser('${user.username}')`;
    $("#all-following").append(`<div class="col-md-6 side-by-side-card">\ 
                           <div class="card">\ 
                           <div class="card-section">\ 
                                <div ${searcUser} class="row onHoverUserSearch">\ 
                                    <div class="col-xs-3"><span class="glyphicon glyphicon-user vecjiGlyph"></span></div>\ 
                                    <div class="col-xs-9 text-left"><h3>${
        user.username
        }</h3></div>\ 
                                    </div>\ 
                                <div class="row">\ 
                                    <div class="col-sm-6 col-xs-3"><span class="drugaBarva">Following: ${
        user.following.length
        }</span></div>\ 
                                    <div class="col-sm-6 col-xs-3"><span class="drugaBarva">Followers: ${
        user.followers.length
        }</span></div>\ 
                                    </div>\ 
                           </div>\ 
                           <div class="card-section"><span>${
        user.description.body
        }</span></div>\ 
                           <div class="card-section" style="text-align: center"><btn class="btn"style="width: 95%" onclick="unFollwej(event, '${user._id}')">Unfollow</btn></div>
                           </div>\ 
                            </div>`);
}


$(document).ready(function () {
  
 getUser(activeUser, function(err, user){
      if(err){
          console.log(err);
      }else{
          //console.log(user);
        $(".activeUser-name").text(user.username);
        $(".activeUser-description").text(user.description.body);
        $(".activeUser-numFollowgin").text(user.following.length);
        $(".activeUser-numFollowers").text(user.followers.length);
      }
  })  
    $("#goGome").click(function () {
        window.location.href = "/home";
    });

    //pridobi vse uporabnike, ki jih aktivni uporabnik followa in implementiraj moznost, da jim neha sledit.
    var all_followers = function (user_id) {
        getAllFollowing(user_id, function(err, users){
            if(err){
                console.log(er);
            }else{
                console.log(users);
                for(let j = 0; j < users.length; j++){
                    displayFollowing(users[j]);
                }
            }
        });
    };

    all_followers(activeUser);
});
