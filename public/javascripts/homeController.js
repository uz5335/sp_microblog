//ko prides na home page preveri ali je kateri uporabnik prijavljen.
"use strict";
//sessionStorage.removeItem("activeUser");
if (!sessionStorage.getItem("activeUser")) {
    //window.location.replace("/login");
    window.location.href = "/login";
    console.log("preusmerjam");
}
var activeUser = sessionStorage.getItem("activeUser"); //iz tega session controllerja dobimo trenutno prijavljenega uporabnika in vse njegove podatke in tweete.
console.log(activeUser);

//ce je kateri prijavljen se koda izvaja naprej. drugace je pa zograj napisan redirect na login screen.

function followejOdstrani(event, user_id) {
    //  --> koda za odstranitev.
    //najdi po DOM drevesu userja katerega bomo sledili
    console.log(user_id)
    userFollow(user_id, activeUser, function(err, ok){
        if(err){
            console.log(err);
        }else{
        
            all_userFeed(activeUser)
            getUser(activeUser, function(err, user){
                if(err){
                    console.log(err);
                }else{
                    $(".activeUser-numFollowgin").text(user.following.length);
                }
            })  
            
        }
    })
    
    $(event.currentTarget)
                .parent()
                .parent()
                .remove(); //odstrani starsa.

}

function userRecommendations(user){
    let searcUser = `onclick=searchClickedUser('${user.username}')`;
    $("#recommendations").append(`<div class="card">\ 
                                    <div class="card-section">\ 
                                        <div ${searcUser} class="row onHoverUserSearch"><div class="col-xs-3"><span class="glyphicon glyphicon-user vecjiGlyph"></span></div><div class="col-xs-9 text-left"><h3>${
        user.username
        }</h3></div></div>\ 
                                            <div class="row"><div class = "col-sm-6 col-xs-3"><span class="drugaBarva">Following: ${
        user.following.length
        }</span></div>\ 
                                            <div class="col-sm-6 col-xs-3"><span class="drugaBarva">Followers: ${
        user.followers.length
        }</span></div></div>\ 
                                        </div>\ 
                                    <div class="card-section" style="text-align: center"><btn class="btn", style="width: 95%" onclick="followejOdstrani(event, '${user._id}')">Follow</btn></div>\ 
                                </div`);
}

$(document).ready(function () {
    
  getUser(activeUser, function(err, user){
      if(err){
          console.log(err);
      }else{
          //console.log(user);
        $(".activeUser-name").text(user.username);
        $(".activeUser-description").text(user.description.body);
        $(".activeUser-numFollowgin").text(user.following.length);
        $(".activeUser-numFollowers").text(user.followers.length);
      }
  })  
    $("#goGome").click(function () {
        window.location.href = "/home";
    });
    
    //prikazi vsa priporocila, vendar samo tista, ki jih uporabnik ze ne followa
    var all_recommendations = function (user_id) {
        getAllRecommendation(user_id, function(err, allUsers){
            if(err){
                console.log(err);
            }else{
                console.log(allUsers);
                for(let i = 0; i < allUsers.length; i++){
                    if(allUsers[i]._id == user_id){
                        continue;
                    }
                    userRecommendations(allUsers[i]);
                }
            }
        });
    
    };

    //$("#recommendations").html("");
    all_recommendations(activeUser);
    
    //Od tu naprej dinamicno nalozi posamezen tweet od aktivnega uporabnika. //dinamicno nalozi vse tweete katerim uporabnik sledi in katere je tudi retweetal.
    //malo bo sprehajanja po for zanki.

    //$("#user-feed").html("");
    all_userFeed(activeUser);
    
    
    /////tukaj bo sel del kode, kjer naredim nov tweet, glede na to kaj je uporabnik vnesel.
    $("#btn-new-tweet").click(function (event) {
        event.preventDefault();
        console.log("novo Tweet.");
        var new_tweet = $("#textarea").val();
        if (new_tweet == "") {
            getAlertModal("Tweet cannot be empty. Type something");
        } else if (new_tweet.length > 250) {
            getAlertModal("Tweets can be only up to 250 charachters long");
            $("#textarea").val();
        } else {
            newUserTweet(activeUser, new_tweet, function(err, tweet){
                if(err){
                    console.log(err);
                    $("#textarea").val("");
                }else{
                    //RELOADEJ STRAN, DA SE PRIKAZE SE ENKRAT CEL USER FEED SE ENKRAT
                    $("#textarea").val("");
                    //console.log(tweet);
                    getOneTweet(tweet._id, function(err, oneTweet){
                        if(err){
                            console.log(err);
                        }else{
                           // console.log(oneTweet, "#user-feed");
                            prependTweet(oneTweet, "#user-feed");
                        }
                    })
                }
            });
            
        }
    });

});
